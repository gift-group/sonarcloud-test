$(document).ready(function() {
	
	$.validate({
	    modules : 'location, date, security, file',
	    onModulesLoaded : function() {
	      $('#country').suggestCountry();
	    }
  	});

    setTimeout(function() {
      $("#successMessage").hide('blind', {}, 500)
    }, 7000);


  	$('.cancel').on('click', function() {
  		window.location.href=$(this).attr('rel');
  	})

  	$('.selAll').on('click', function () {
  		$obj = $(this);
  		if($obj.is(':checked')){
  			$obj.parents('thead').siblings('tbody').find('input[name="selData"]').prop('checked', true);;
  		} else {
  			$obj.parents('thead').siblings('tbody').find('input[name="selData"]').removeAttr('checked');
  		}
  	});

  	$('body').on('click', '.delSelected', function() {
  		$obj = $(this);
  		$tabClass = $obj.attr('rel');
  		$base_url = $obj.attr('data-base-url');
  		$arr = [];
  		$('#cnfrm_delete').find('.modal-body').find('input[name="ids"]').remove();
  		$('table.' + $tabClass).find('tbody').find('input[name="selData"]').each(function() {
  			$inpObj = $(this);
  			if($inpObj.is(':checked')){
  				$arr.push($inpObj.val());
  			}
  		});
  		if($arr.length > 0) {
  			//console.log($arr);
  			$('#cnfrm_delete').find('.yes-btn').attr('href', $base_url+$arr.join('-'));
  			$('#cnfrm_delete').modal('show');
  		}
  	});

  /* Script for profile page start here */

  $("#fileUpload").on('change', function () {
    if (typeof (FileReader) != "undefined") {
      var image_holder = $("#image-holder");
      image_holder.empty();
      var reader = new FileReader();
      reader.onload = function (e) {
        $("<img />", {
          "src": e.target.result,
          "class": "thumb-image setpropileam"
        }).appendTo(image_holder);
      }
      image_holder.show();
      reader.readAsDataURL($(this)[0].files[0]);
    } else {
      alert("This browser does not support FileReader.");
    }
  });


  $('#profileSubmit').on('click', function() {
    $res = 1;
    $('div.form-group').each(function() {
      if($(this).hasClass('has-error')){
        $res = 0;
      }
    });
    if($res == 1) {
      $('form').submit();
    }
  })

  $('#profileEmail').bind('change keyup', function() {
    $obj = $(this);
    $obj.parents('div.form-group')
        .removeClass('has-error')
        .find('span.text-red').remove();
    var email = $obj.val();
    var uId = $('[name="id"]').val();
    $.ajax({
      url:  $('body').attr('data-base-url') + 'user/checEmailExist',
      method:'post',
      data:{
        email :email,
        uId : uId
      }
    }).done(function(data) {
      if(data == 0) {
        $obj
        .after('<span class="text-red">This Email Already Exist...</span>')
        .parents('div.form-group')
        .addClass('has-error');
      }
      console.log(data);
    })
  })

  /* Script for profile page End here */

  /* Script for user page start here */
  $('.InviteUser').on('click', function() {
    $('#nameModal_user').find('.box-title').text('Users Invitation');
    $('#nameModal_user').find('.modal-body').html('<form id="send-offer-with-invite" method="post" action="" enctype="multipart/form-data"><div class="row">'+
        '<div class="col-md-12 m-t-20 form-horizontal">'+
          '<div class="row">' +
            '<div class="col-md-12">' +
              '<label for="user_name_by_admin" class="">Full Name <em>*</em></label>'+
              '<input type="text" name="user_name_by_admin" class="form-control" value="" placeholder="Full Name" required="required" />'+              
            '</div>'+
          '</div>'+
          '<div class="row">' +
            '<div class="col-md-12">' +
              '<label for="emails" class="">Email ID <em>*</em></label>'+
              '<input type="email" name="emails" class="form-control" value="" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$" placeholder="Enter Email ID" required="required" />'+              
            '</div>'+
          '</div>'+          
          '<div class="row">' +
            '<div class="col-md-12">' +
              '<input type="submit" class="btn btn-primary pull-right send-btn" value="Send Invitation">'+
            '</div>'+
          '</div>'+          
        '</div>'+
      '</div></form>');
    $('#nameModal_user').modal('show');

    // var today = new Date().toISOString().split('T')[0]; 
    // document.getElementsByName("date_of_joining_by_hr")[0].setAttribute('min', today);
  });

  $('.modal-body').on('submit', 'form#send-offer-with-invite', function(e) {

    e.preventDefault();
    $obj = $('.send-btn');
    $obj.addClass('disabled');
    
    //$obj.html('<i class="fa fa-cog fa-spin"></i> Send Offer');
    $obj.parents('div.row').find('.msg-div').remove();
    $uname = $obj.parents('.modal-body').find('input[name="user_name_by_admin"]').val();
    $emails = $obj.parents('.modal-body').find('input[name="emails"]').val();
    
    var formData = new FormData(this);

    $.ajax({
      url: $('body').attr('data-base-url') + 'user/InvitePeople',
      method:'post',
      data: formData,
      dataType: 'json',
      contentType: false,
      cache: false,
      processData:false,
    }).done(function(data){

      console.log(data);

      if(data) {

        var part = '';
        if(data.noTemplate != 0){
          part = '<p><strong>Info:</strong> '+data.noTemplate+'</p>';
        }
        var add_style = "";
        if(data.existCount > 0){
            add_style = 'style="color: #c73232"';
        }
        
        $obj.parents('.form-horizontal div.row').prepend('<div class="col-md-12 m-t-20 msg-div"><div class="alert alert-info" role="alert">'+
                                '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>'+
                                '<div class="msg-p">'+
                                '<p><strong>Success:</strong> '+data.successCount+' Invitation sent successfully</p>'+
                                '<p '+ add_style +'><strong>Info:</strong> '+data.existCount+' Email already present in database</p>'+
                                '<p><strong>Info:</strong> '+data.invalidEmailCount+' Invalid email found</p>'+part+
                                '</div>'+
                                '</div>'+
                              '</div>');
        //$obj.html('Send Offer');

        $obj.removeClass('disabled');

        if(data.successCount > 0){          
          $obj.parents('.modal-body').find('input[name="user_name_by_admin"]').val('');
          $obj.parents('.modal-body').find('input[name="emails"]').val('');
        }
      }
    }).error(function(error){
      
      $obj.removeClass('disabled');
      $obj.parents('.form-horizontal div.row').prepend('<div class="col-md-12 m-t-20 msg-div">'+
                                                      '<div class="alert alert-danger" role="alert">'+
                                                        '<p>ERROR: Mail not accepted from server. Please contact administrator.</p>'+
                                                      '</div>'+
                                                      '</div>');
    });
  });   

  $(".content-wrapper").on("click",".modalButtonUser", function(e) {
    $.ajax({
      url : $('body').attr('data-base-url') + 'user/get_modal',
      method: 'post', 
      data : {
        id: $(this).attr('data-src')
      }
    }).done(function(data) {
      $('#nameModal_user').find('.box-title').text('User Form');
      $('#nameModal_user').find('.modal-body').html(data);
      $('#nameModal_user').modal('show'); 
    })
  });

/*  $("#nameModal_user").on("hidden.bs.modal", function(){
    $(this).find("iframe").html("");
    $(this).find("iframe").attr("src", "");
    });*/
  /* Script for user page end here */

  /* Script for Templates Starts here */
    $('.box-body').on('click', '.view_template', function() {
      $obj = $(this);
      $tmp_id = $obj.attr('data-src');
      $.ajax({
        url: $('body').attr('data-base-url') + "templates/preview",
        method:'post',
        data:{
          template_id: $tmp_id
        }
      }).done(function(data) {
        $('#previewModal').find('div.modal-body').html(data);
        $('#previewModal').modal('show');
        $('#previewModal').find('a').attr('href', 'javascript:;');
      });
    });

  $(".content-wrapper").on("click",".templateModalButton", function(e) {  
    $.ajax({
      url : $('body').attr('data-base-url') + "templates/modal_form",
      method: "post", 
      data : {
      id: $(this).attr("data-src")
      }
      }).done(function(data) {
      $("#nameModal_Templates").find(".modal-body").html(data);
      $("#nameModal_Templates").modal("show"); 
    })
  });
  /* Script for Templates End here */
});

function setId(id, module) {
  var url =  $('body').attr('data-base-url');
  $("#cnfrm_delete").find("a.yes-btn").attr("href",url+"/"+ module +"/delete/"+id);
}

function resizeIframe(obj) { 
  obj.style.height = obj.contentWindow.document.body.scrollHeight + "px";
}