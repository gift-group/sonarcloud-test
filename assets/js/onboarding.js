$(document).ready(function () {

//    var dateToday = new Date();
//    $( ".date-picker-for-birth-date" ).datepicker({ 
//        dateFormat: 'dd/mm/yy',
//        maxDate: dateToday,
//        changeMonth: true,
//        changeYear: true,
//        yearRange: "-100:+0",
//    });

    $(document).on('change', '#dob-of-emp', validatedateformat);

    function validatedateformat() {
        
        // good input
        var dob = new Date($(this).val());
        var today = new Date();
        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
        $('#calculate-age-from-dob').val(age);
    };

    //Family Members list
    var counter_for_members = $('.family-member-details .box-div-wrapper').length;
    $("#addFamilyMemberButton").click(function () {

        counter_for_members = $('.family-member-details .box-div-wrapper').length;
        var newTextBoxDiv = $(document.createElement('div')).attr("class", 'box-div-wrapper');

        newTextBoxDiv.after().html('<div class="glyphicon glyphicon-remove custom-remove-button"></div><div class="row">' +
                '<div class="col-md-12"><label class="control-label text-bold">Family Member: </label></div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">First Name <em>*</em></label>' +
                '<input  maxlength="100" type="text" name="member_fname[]" required="required" class="form-control" placeholder="First Name"  />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Middle Name</label>' +
                '<input maxlength="100" type="text" name="member_mname[]" class="form-control" placeholder="Middle Name" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Last Name <em>*</em></label>' +
                '<input maxlength="100" type="text" name="member_lname[]" required="required" class="form-control" placeholder="Last Name" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Relationship <em>*</em></label>' +
                '<select name="member_relationship[]" class="form-control" required="required">' +
                '<option value="" disabled="disabled" selected="selected">Select Relationship</option>' +
                '<option value="Father">Father</option>' +
                '<option value="Mother">Mother</option>' +
                '<option value="Spouse">Spouse</option>' +
                '<option value="Son">Son</option>' +
                '<option value="Daughter">Daughter</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Date of Birth <em>*</em></label>' +
                '<input type="date" name="member_date_of_birth[]" required="required" pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" class="date-picker-for-birth-date form-control" placeholder="DD/MM/YYYY" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label occupation-required">Occupation <em>*</em></label>' +
                '<input maxlength="100" type="text" name="member_occupation[]" class="form-control family-member-occupation" placeholder="Occupation" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group upload-files-custom">' +
                '<label class="control-label">Aadhaar Card Front</label>' +
                '<input type="text" name="member_aadhaar_card_front_org[]" class="form-control" readonly />' +
                '<input type="file" name="member_aadhaar_card_front[]" class="form-control" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group upload-files-custom">' +
                '<label class="control-label">Aadhaar Card Back</label>' +
                '<input type="text" name="member_aadhaar_card_back_org[]" class="form-control" readonly />' +
                '<input type="file" name="member_aadhaar_card_back[]" class="form-control" />' +
                '</div>' +
                '</div>' +
                '</div>');

        newTextBoxDiv.appendTo("#family-boxes-group");
        counter_for_members++;
    });

    // $("#removeFamilyMemberButton").click(function () {
    //     if (counter_for_members == 1) {
    //         alert("Don't remove all the fields.");
    //         return false;
    //     }

    //     counter_for_members--;
    //     $("#family-box-div" + counter_for_members).remove();
    // });


    $("#EduQualificationBoxesGroup").on("change", "select.form-control", function () {
                
        if ($(this).val() == "Other") {
            $(this).closest('.box-div-wrapper').find(".edu-other-qualification input").prop('required', true);
            $(this).closest('.box-div-wrapper').find(".edu-other-qualification").show();
        } else {
            $(this).closest('.box-div-wrapper').find(".edu-other-qualification input").prop('required', false);
            $(this).closest('.box-div-wrapper').find(".edu-other-qualification").hide();
        }
    });

    $("#family-boxes-group").on("change", "select.form-control", function () {
                        
        if ($(this).val() == "Children" || $(this).val() == "Son" || $(this).val() == "Daughter") {
            $(this).closest('.box-div-wrapper').find("input.family-member-occupation").prop('required', false);
            $('.occupation-required em').hide();
        } else {
            $(this).closest('.box-div-wrapper').find("input.family-member-occupation").prop('required', true);
            $('.occupation-required em').show();
        }
    });

    var edu_counter = 1;
    $("#addDocsButton").click(function () {

        //var newTextBoxDiv = $(document.createElement('div')).attr("id", 'BoxDiv' + edu_counter).attr("class", 'box-div-wrapper');
        var newTextBoxDiv = $(document.createElement('div')).attr("class", 'box-div-wrapper');

        newTextBoxDiv.after().html('<div class="glyphicon glyphicon-remove custom-remove-button"></div><div class="row"><div class="col-md-12"><label class="control-label text-bold">Educational Qualification: </label></div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Qualification <em>*</em></label>' +
                '<select name="edu_qualification[]" class="form-control" required="required">' +
                '<option value="" disabled="disabled" selected="selected">Select Qualification</option>' +
                '<option value="SSC">SSC</option>' +
                '<option value="HSC">HSC</option>' +
                '<option value="Graduate">Graduate</option>' +
                '<option value="Post Graduate">Post Graduate</option>' +
                '<option value="Other">Other</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4 edu-other-qualification hide-other-field-for-other-case">' +
                '<div class="form-group">' +
                '<label class="control-label">Qualification Name <em>*</em></label>' +
                '<input type="text" name="edu_qualification_other[]" class="form-control other-field-input" placeholder="Qualification" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">University</label>' +
                '<input type="text" name="edu_qualification_university[]" class="form-control" placeholder="University" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="row">' +
                '<div class="col-md-6">' +
                '<div class="form-group">' +
                '<label class="control-label">Passing Year</label>' +
                '<select name="edu_qualification_passing_year[]" id="selectEduPassingYear-'+ edu_counter +'" class="form-control"></select>' + 
                '</div>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<div class="form-group">' +
                '<label class="control-label">Marks % or Grade</label>' +
                '<input type="text" name="edu_qualification_percentage_marks[]" class="form-control" placeholder="Marks % or Grade" />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Specialization</label>' +
                '<input type="text" name="edu_qualification_specialization[]" class="form-control" placeholder="Specialization" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group upload-files-custom">' +
                '<label class="control-label">Upload File <em>*</em></label>' +
                '<input type="text" name="edu_qualification_docs_org[]" class="form-control" readonly />' +
                '<input type="file" name="edu_qualification_docs[]" class="form-control" required="required" />' +
                '</div>' +
                '</div>');

        newTextBoxDiv.appendTo("#EduQualificationBoxesGroup");
        
        var max = new Date().getFullYear(),
            min = max - 59,
            selectEduPassingYear = document.getElementById('selectEduPassingYear-'+edu_counter);

        for (var i = max; i >= min; i--){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            selectEduPassingYear.appendChild(opt);
        }
        
        edu_counter++;
    });

    var increment_counter = 1;
    $("#addProfDocsButton").click(function () {

        var newTextBoxDiv = $(document.createElement('div')).attr("class", 'box-div-wrapper');

        newTextBoxDiv.after().html('<div class="glyphicon glyphicon-remove custom-remove-button"></div><div class="row"><div class="col-md-12"><label class="control-label text-bold">Professional Qualification: </label></div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Qualification</label>' +
                '<input type="text" name="pro_qualification[]" class="form-control" placeholder="Qualification" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">University</label>' +
                '<input type="text" name="pro_qualification_university[]" class="form-control" placeholder="University" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Passing Year</label>' +
                '<select name="pro_qualification_passing_year[]" id="selectProPassingYear-'+ increment_counter +'" class="form-control"></select>' +                
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Marks % or Grade</label>' +
                '<input type="text" name="pro_qualification_percentage_marks[]" class="form-control" placeholder="Marks % or Grade" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Specialization</label>' +
                '<input type="text" name="pro_qualification_specialization[]" class="form-control" placeholder="Specialization" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group upload-files-custom">' +
                '<label class="control-label">Upload File</label>' +
                '<input type="text" name="pro_qualification_docs_org[]" class="form-control" readonly />' +
                '<input type="file" name="pro_qualification_docs[]" class="form-control" />' +
                '</div>' +
                '</div>');

        newTextBoxDiv.appendTo("#ProfQualificationBoxesGroup");
        
        var max = new Date().getFullYear(),
            min = max - 59,
            selectProPassingYear = document.getElementById('selectProPassingYear-'+increment_counter);

        for (var i = max; i >= min; i--){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            selectProPassingYear.appendChild(opt);
        }
        
        increment_counter++;
    });

    var exp_counter = 1;
    $("#addExperienceButton").click(function () {
        
        var newTextBoxDiv = $(document.createElement('div')).attr("class", 'box-div-wrapper');

        newTextBoxDiv.after().html('<div class="glyphicon glyphicon-remove custom-remove-button"></div><div class="row">' +
                '<div class="col-md-12"><label class="control-label text-bold">Experience: </label></div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Organization <em>*</em></label>' +
                '<input type="text" name="experience_organization[]" required="required" class="form-control" placeholder="Organization" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="row">' +
                '<div class="col-md-6">' +
                '<div class="row">' +
                '<div class="col-md-12"><label class="control-label">Period From <em>*</em></label></div>' +
                '<div class="col-md-6 col-xs-6">' +
                '<div class="form-group">' +
                '<select name="experience_period_from_year[]" id="selectFromYear-'+ exp_counter +'" required="required" class="form-control period-from-year"></select>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 col-xs-6">' +
                '<div class="form-group">' +
                '<select name="experience_period_from_month[]" class="form-control period-from-month" required="required">' +
                '<option value="" disabled="disabled" selected="selected">Month</option>' +
                '<option value="1">Jan</option>' +
                '<option value="2">Feb</option>' +
                '<option value="3">Mar</option>' +
                '<option value="4">Apr</option>' +
                '<option value="5">May</option>' +
                '<option value="6">Jun</option>' +
                '<option value="7">Jul</option>' +
                '<option value="8">Aug</option>' +
                '<option value="9">Sept</option>' +
                '<option value="10">Oct</option>' +
                '<option value="11">Nov</option>' +
                '<option value="12">Dec</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<div class="row">' +
                '<div class="col-md-12"><label class="control-label">Period To <em>*</em></label></div>' +
                '<div class="col-md-6 col-xs-6">' +
                '<div class="form-group">' +
                '<select name="experience_period_to_year[]" id="selectToYear-'+ exp_counter +'" required="required" class="form-control period-to-year"></select>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 col-xs-6">' +
                '<div class="form-group">' +
                '<select name="experience_period_to_month[]" class="form-control period-to-month" required="required">' +
                '<option value="" disabled="disabled" selected="selected">Month</option>' +
                '<option value="1">Jan</option>' +
                '<option value="2">Feb</option>' +
                '<option value="3">Mar</option>' +
                '<option value="4">Apr</option>' +
                '<option value="5">May</option>' +
                '<option value="6">Jun</option>' +
                '<option value="7">Jul</option>' +
                '<option value="8">Aug</option>' +
                '<option value="9">Sept</option>' +
                '<option value="10">Oct</option>' +
                '<option value="11">Nov</option>' +
                '<option value="12">Dec</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div> ' +
                '</div>' +
                '<div class="col-md-4">' +
                    '<div class="form-group">' +
                        '<label class="control-label">Total Experience(Months)</label>' +
                        '<input type="text" name="total_experience[]" value="" class="form-control total-experience-single-org" placeholder="Total Experience" readonly />' +
                    '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Designation <em>*</em></label>' +
                '<input type="text" name="experience_designation[]" required="required" class="form-control" placeholder="Designation" />' +
                '</div>' +
                '</div>' +
                // '<div class="col-md-4">' +
                // '<div class="form-group">' +
                // '<label class="control-label">Job Responsibility <em>*</em></label>' +
                // '<input type="text" name="experience_job_responsibility[]" required="required" class="form-control" placeholder="Job Responsibility" />' +
                // '</div>' +
                // '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Gross Salary/Month <em>*</em></label>' +
                '<input type="text" name="experience_gross_salary_month[]" required="required" class="form-control" placeholder="Gross Salary/Month" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label class="control-label">Reason For Leaving <em>*</em></label>' +
                '<input type="text" name="experience_reason_for_leaving[]" required="required" class="form-control" placeholder="Reason For Leaving" />' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group upload-files-custom">' +
                    '<label class="control-label">Relieving Letter</label>' +
                    '<input type="text" name="experience_relieving_letter_org[]" value="" class="form-control" readonly />' +
                    '<input type="file" name="experience_relieving_letter[]" class="form-control" />' +
                '</div>' +
            '</div>' +
            '<div class="col-md-4">' +
                '<div class="form-group upload-files-custom">' +
                    '<label class="control-label">Experience Certificate</label>' +
                    '<input type="text" name="experience_certificate_org[]" value="" class="form-control" readonly />' +
                    '<input type="file" name="experience_certificate[]" class="form-control" />' +
                '</div>' +
            '</div>' +
            '</div>');

        newTextBoxDiv.appendTo("#ExperienceBoxesGroup");

        var max = new Date().getFullYear(),
            min = max - 59,
            selectFrom = document.getElementById('selectFromYear-'+exp_counter);
            selectTo = document.getElementById('selectToYear-'+exp_counter);

        for (var i = max; i >= min; i--){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            selectFrom.appendChild(opt);
        }

        for (var j = max; j >= min; j--){
            var opt = document.createElement('option');
            opt.value = j;
            opt.innerHTML = j;
            selectTo.appendChild(opt);
        }

        exp_counter++;
    });

    $(document).on('click', '.custom-remove-button', function (e) {
        var $form = $(this).closest('.box-div-wrapper');
        e.preventDefault();
               
        $('#confirm-field-remove').modal({
            
        }).on('click', '#delete-field-record', function (event) {
            $form.remove();
        });
    });

    $(document).on('change', 'select[name="marital_status"]', function () {
        if ($(this).val() == "Married") {            
            $('#spouse-information input[name="spouse_fname"]').prop('required', true);
            $('#spouse-information input[name="spouse_lname"]').prop('required', true);
            $('#spouse-information').show();
        } else {
            $('#spouse-information input[name="spouse_fname"]').prop('required', false);
            $('#spouse-information input[name="spouse_lname"]').prop('required', false);
            
            $('#spouse-information input[name="spouse_fname"]').val('');
            $('#spouse-information input[name="spouse_mname"]').val('');
            $('#spouse-information input[name="spouse_lname"]').val('');
            $('#spouse-information').hide();
        }
    });

    $(document).on('change', 'select[name="nationality"]', function () {
        if ($(this).val() == "Indian") {
            
            $('.setup-content select[name="pan_card_id"]').prop('required', true);
            $('.setup-content select[name="aadhaar_card"]').prop('required', true);
            
            $('.nri-employee em').show();
            
            var pan_card = $('select[name="pan_card_id"]').val();
            if(pan_card == "Having"){
                $('.setup-content input[name="pan_number"]').prop('required', true);   
                $('.nri-employee-panid em').show();
            }else{
                $('.setup-content input[name="pan_number"]').prop('required', false);   
                $('.nri-employee-panid em').hide();
            }

            var aadhaar_card = $('select[name="aadhaar_card"]').val();
            if(aadhaar_card == "Having"){            
                $('.setup-content input[name="aadhaar_number"]').prop('required', true);
                $('.nri-employee-aadhaarid em').show();
            }else{
                $('.setup-content input[name="aadhaar_number"]').prop('required', false);
                $('.nri-employee-aadhaarid em').hide();
            }
            
            $('.setup-content input[name="passport_number"]').prop('required', false);
            $('.setup-content input[name="passport_issue_date"]').prop('required', false);
            $('.setup-content input[name="passport_expiry_date"]').prop('required', false);
            $('.nri-employee-passport em').hide();
        } else {            
            $('.setup-content select[name="pan_card_id"]').prop('required', false);
            $('.setup-content select[name="aadhaar_card"]').prop('required', false);
            
            $('.setup-content input[name="pan_number"]').prop('required', false);
            $('.setup-content input[name="aadhaar_number"]').prop('required', false); 
            
            $('.nri-employee em, .nri-employee-panid em, .nri-employee-aadhaarid em').hide();

            $('.setup-content input[name="passport_number"]').prop('required', true);
            $('.setup-content input[name="passport_issue_date"]').prop('required', true);
            $('.setup-content input[name="passport_expiry_date"]').prop('required', true);
            $('.nri-employee-passport em').show();
        }
    });
    
    $(document).on('change', 'select[name="pan_card_id"]', function () {
        var pan_card = $(this).val();
        if(pan_card == "Having"){
            $('.setup-content input[name="pan_number"]').prop('required', true);   
            $('.nri-employee-panid em').show();
        }else{
            $('.setup-content input[name="pan_number"]').prop('required', false);   
            $('.nri-employee-panid em').hide();
        }
    });
    
    $(document).on('change', 'select[name="aadhaar_card"]', function () {
        var aadhaar_card = $(this).val();
        if(aadhaar_card == "Having"){
            $('.setup-content input[name="aadhaar_number"]').prop('required', true);   
            $('.nri-employee-aadhaarid em').show();
        }else{
            $('.setup-content input[name="aadhaar_number"]').prop('required', false);   
            $('.nri-employee-aadhaarid em').hide();
        }
    });
    
    $(document).on('change', 'select[name="pf_number_exist"]', function () {
        var pf_number = $(this).val();
        if(pf_number == "Yes"){            
            $('.setup-content input[name="pf_number"]').prop('required', true);
            $('.setup-content input[name="uan_number"]').prop('required', true);
            $('.employee-pf-number em').show();
        }else{
            $('.setup-content input[name="pf_number"]').prop('required', false);
            $('.setup-content input[name="uan_number"]').prop('required', false);
            $('.employee-pf-number em').hide();
        }
    });
    
    // $(document).on('focusout', '#date-of-joining', function() {
    //     var dateString = $(this).val();
    //     var myDate = new Date(dateString);
    //     var today = new Date();
    //     if ( myDate < today ) {
	// 		$(this).val('');
	// 		$('#popup-alert-box .modal-body').text("Joining date cannot be less than today's date.");
    //         $('#popup-alert-box').modal('show');
    //         return false;
    //     }
    //     return true;
    // });

    if($('#content-outer-div').hasClass('onboarding-page')){

        var selected_option = $('select[name="marital_status"]').val();
        if(selected_option == "Married"){
            $('#spouse-information input[name="spouse_fname"]').prop('required', true);
            $('#spouse-information input[name="spouse_lname"]').prop('required', true);
            $('#spouse-information').show();
        }

        var selected_nationality = $('select[name="nationality"]').val();
        if(selected_nationality == "Indian"){
            $('.setup-content input[name="pan_card_id"]').prop('required', true);
            $('.setup-content input[name="aadhaar_card"]').prop('required', true);                    
            $('.nri-employee em').show();
        }else{
            $('.setup-content input[name="passport_number"]').prop('required', true);
            $('.setup-content input[name="passport_issue_date"]').prop('required', true);
            $('.setup-content input[name="passport_expiry_date"]').prop('required', true);
            $('.nri-employee-passport em').show();
        }
        
        var pan_card = $('select[name="pan_card_id"]').val();
        if(pan_card == "Having"){
            $('.setup-content input[name="pan_number"]').prop('required', true);   
            $('.nri-employee-panid em').show();
        }else{
            $('.setup-content input[name="pan_card"]').prop('required', false);
        }
        
        var aadhaar_card = $('select[name="aadhaar_card"]').val();
        if(aadhaar_card == "Having"){            
            $('.setup-content input[name="aadhaar_number"]').prop('required', true);
            $('.nri-employee-aadhaarid em').show();
        } else {
            $('.setup-content input[name="aadhaar_card_front"]').prop('required', false);
        }
        
        var pf_number = $('select[name="pf_number_exist"]').val();
        if(pf_number == "Yes"){            
            $('.setup-content input[name="pf_number"]').prop('required', true);
            $('.setup-content input[name="uan_number"]').prop('required', true);
            $('.employee-pf-number em').show();
        }
    }
    
    $(document).on('change', '.period-to-year, .period-to-month, .period-from-month, .period-from-year', function(){
        
        var fromMonth = $(this).closest('.box-div-wrapper').find('.period-from-month').val();
        var fromYear = $(this).closest('.box-div-wrapper').find('.period-from-year').val();
        var toMonth = $(this).closest('.box-div-wrapper').find('.period-to-month').val();
        var toYear = $(this).closest('.box-div-wrapper').find('.period-to-year').val();
       
        if(toMonth !== null || toYear !== null){
            var monthsinYear = (toYear - fromYear) * 12;
            var months = toMonth - fromMonth;
            var totalMonths = monthsinYear + months + 1;

            if(totalMonths <= 0){
                $('#popup-alert-box .modal-body').text('Please enter valid experience.');
                $('#popup-alert-box').modal('show');
                
                $(this).closest('.box-div-wrapper').find('.period-to-month').val(fromMonth);
                $(this).closest('.box-div-wrapper').find('.period-to-year').val(fromYear);

                totalMonths = 1;
            }

            $(this).closest('.box-div-wrapper').find('.total-experience-single-org').val(totalMonths);
        }
    });
    
    $(document).on('focusout', '#reference-phone-0', function() {
        
        var refer_1_phone = $(this).val();
        var refer_2_phone = $('#reference-phone-1').val();
        
        if(refer_1_phone == refer_2_phone){
            $('#popup-alert-box .modal-body').text('Please enter two different reference number.');
            $('#popup-alert-box').modal('show');
            $(this).val('');
        }
    });    
    
    $(document).on('focusout', '#reference-phone-1', function() {
        
        var refer_1_phone = $(this).val();
        var refer_2_phone = $('#reference-phone-0').val();
        
        if(refer_1_phone == refer_2_phone){            
            $('#popup-alert-box .modal-body').text('Please enter two different reference number.');
            $('#popup-alert-box').modal('show');
            $(this).val('');
        }
    });
    
    $(document).on("change", "input[type='file']", function () {
        //console.log(this.files[0].size);
        $('#send-offer-with-invite .invite-errors, #send-revised-offer-with-invite .invite-errors').hide();

        if(this.files[0].size > 2000000) {
            $('#popup-alert-box .modal-body').text('Please upload less than 2MB size file. Thanks!');
            $('#popup-alert-box').modal('show');  
            $('#send-offer-with-invite .invite-errors, #send-revised-offer-with-invite .invite-errors').show();          
            $(this).val('');
        }
    });
        
    //Next and Prev button check validations
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        $('.content .alert').hide();

        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='date'],input[type='month'],input[type='text'],input[type='email'],input[type='tel'],input[type='file'],select,input[type='url'],textarea[textarea],textarea"),
                isValid = true;
        
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {

                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    allPrevBtn.click(function () {
        $('.content .alert').hide();

        var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],textarea[textarea]");                

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content');

    //Current steps
    var current_step = $('#current-step').val();
    allWells.hide();
    $('#step-'+current_step).show();

    var stepwizardTarget = 'a[href="#step-'+current_step+'"]';
    
    navListItems.removeClass('btn-primary').addClass('btn-default');
    
    for(var i=1; i <= current_step; i++){
        $('a[href="#step-'+i+'"]').removeAttr("disabled");
    }

    $(stepwizardTarget).removeAttr("disabled");
    $(stepwizardTarget).addClass('btn-primary');;
    $(stepwizardTarget).find('input:eq(0)').focus();
});