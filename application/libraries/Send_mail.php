<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   /**
     * Send_mail class is initiating phpmailer
     */
class Send_mail{
    function __construct() {
		 require './application/libraries/phpmailer/class.phpmailer.php';
    }

   /**
     * This function is used to send mail
     * @return true
     * @Param : $sub - Subject of mail
     * @Param : $body - mail body contant
     * @Param : $email - To email address
     * @Param : $smtp - smtp setting array
     */
	function email($sub, $body, $email, $smtp, $remail=array()){

        $fileName = 'email_log.txt';    
        $stringEmailLog = "\n\nLog Date: ". date("l jS \of F Y h:i:s A");
        
        $stringEmailLog .= " | Sub: ".$sub." | Email: ".$email;        

        $SMTP_EMAIL = isset($smtp['SMTP_EMAIL'])?$smtp['SMTP_EMAIL']:'';
        $HOST = isset($smtp['HOST']) ? $smtp['HOST'] : '';
        $PORT = isset($smtp['PORT']) ? $smtp['PORT'] : '';
        $SMTP_SECURE = isset($smtp['SMTP_SECURE']) ? $smtp['SMTP_SECURE'] :'';
        $SMTP_PASSWORD = isset($smtp['SMTP_PASSWORD']) ? $smtp['SMTP_PASSWORD'] :''; 
        $EmailFromName = isset($smtp['company_name']) ? $smtp['company_name'] :''; 
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 1;
        $mail->Debugoutput = 'text';
        $mail->Host = $HOST;
        $mail->Port = $PORT;
        $mail->SMTPSecure = $SMTP_SECURE;
        $mail->SMTPAuth = true;
        $mail->Username = $SMTP_EMAIL;
        $mail->Password = $SMTP_PASSWORD;
        // $mail->setFrom($SMTP_EMAIL, $EmailFromName);
        $mail->setFrom('cs@bostonbaskets.us', $EmailFromName);
        // $mail->addReplyTo($SMTP_EMAIL, '');
        $mail->addReplyTo('cs@bostonbaskets.us');
        $mail->addAddress("$email");
 
        if(!empty($remail)){
            foreach($remail as $key => $val){
                $mail->AddCC($val);

                $stringEmailLog .= " | CC: ". $key ." : ". $val;
            }
        }
        $mail->Subject = $sub;
        $mail->msgHTML($body);
        $mail->AltBody = 'This is a confirmation message';
        if (!$mail->send()) {
            $stringEmailLog .= " | Error: ". $mail->ErrorInfo;

            $fh = fopen($fileName, 'a') or die("Can't open file.");
            fwrite($fh, $stringEmailLog);
            fclose($fh);
            return false;
        } else {
            $stringEmailLog .= " | Success: Mail send.";

            $fh = fopen($fileName, 'a') or die("Can't open file.");
            fwrite($fh, $stringEmailLog);
            fclose($fh);
            return true;
        }
    }   
}
?>