<?php
defined('BASEPATH') OR exit('No direct script access allowed ');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('User_model');
        $this->user_id = isset($this->session->get_userdata()['user_details'][0]->id) ? $this->session->get_userdata()['user_details'][0]->users_id : '1';
    }

    /**
     * This function is redirect to users profile page
     * @return Void
     */
    public function index() {
        if (is_login()) {
            redirect(base_url() . 'user/profile', 'refresh');
        }
    }

    /**
     * This function is used to load login view page
     * @return Void
     */
    public function login() {
        if (isset($_SESSION['user_details'])) {
            redirect(base_url() . 'user/profile', 'refresh');
        }
        $this->load->view('include/script');
        $this->load->view('login');
    }

    /**
     * This function is used to logout user
     * @return Void
     */
    public function logout() {
        is_login();
        $this->session->unset_userdata('user_details');
        redirect(base_url() . 'user/login', 'refresh');
    }

    /**
     * This function is used to register user
     * @return Void
     */
    public function registration() {
        
        if (isset($_SESSION['user_details'])) {
            redirect(base_url() . 'user/profile', 'refresh');
        }

        //Check if admin allow to registration for user
        if (setting_all('register_allowed') == 1) {
            
            if ($this->input->post()) {
                                
                if($this->checkInvitationForRegistration($this->input->post('email'))){
                    $this->add_edit();
                    $this->session->set_flashdata('messagePr', 'Successfully Registered..');
                } else {
                    $this->session->set_flashdata('messagePr', 'Your email address is not registered with us.');
                    $this->load->view('include/script');
                    $this->load->view('register');
                }
                
            } else {

                $this->load->view('include/script');
                $this->load->view('register');
            }
        } else {
           
            $this->session->set_flashdata('messagePr', 'Registration not allowed..');
            redirect(base_url() . 'user/login', 'refresh');
        }
    }

    /**
     * This function is used for user authentication ( Working in login process )
     * @return Void
     */
    public function auth_user($page = '') {

        $return = $this->User_model->auth_user();

        if (empty($return)) {
            $this->session->set_flashdata('messagePr', 'Invalid credentials.');
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            if ($return == 'not_varified') {
                $this->session->set_flashdata('messagePr', 'This account is not verified. Please contact to your admin.');
                redirect(base_url() . 'user/login', 'refresh');
            } else {
                
                $this->session->set_userdata('user_details', $return);
                $lastLogin = $this->User_model->lastLoginInfo($return[0]->users_id);
                
                $sessionArray = array('users_id'=>$return[0]->users_id,                    
                                        'user_type'=>$return[0]->user_type,
                                        'name'=>$return[0]->name,
                                        'lastLogin'=> $lastLogin[0]['createdDtm'],
                                        'isLoggedIn' => TRUE
                                );

                $this->session->set_userdata('last_login_info', $sessionArray);
                
                unset($sessionArray['users_id'], $sessionArray['isLoggedIn'], $sessionArray['lastLogin']);

                $loginInfo = array("users_id"=>$return[0]->users_id, "sessionData" => json_encode($sessionArray), "machineIp"=>$_SERVER['REMOTE_ADDR'], "userAgent"=>getBrowserAgent(), "agentString"=>$this->agent->agent_string(), "platform"=>$this->agent->platform(), "is_active"=>1);

                $this->User_model->lastLogin($loginInfo);
            }
            
            redirect(base_url() . 'user/profile', 'refresh');
        }
    }

    /**
     * This function is used send mail in forget password
     * @return Void
     */
    public function forgetpassword() {
        
        $page['title'] = 'Forgot Password';
        if ($this->input->post()) {
            
            $setting = settings();
            $res = $this->User_model->get_data_by('users', $this->input->post('email'), 'email', 1);

            if (isset($res[0]->users_id) && $res[0]->users_id != '' && $res[0]->status == 'active') {
                
                $var_key = $this->getVarificationCode();
                $this->User_model->updateRow('users', 'users_id', $res[0]->users_id, array('var_key' => $var_key));
                $sub = "Reset password";
                $email = $this->input->post('email');
                $data = array(
                    'user_name' => $res[0]->name,
                    'action_url' => base_url(),
                    'sender_name' => $setting['company_name'],
                    'website_name' => $setting['website'],
                    'varification_link' => base_url() . 'user/mail_varify?code=' . $var_key,
                    'url_link' => base_url() . 'user/mail_varify?code=' . $var_key,
                );
                $body = $this->User_model->get_template('forgot_password');
                $body = $body->html;
                foreach ($data as $key => $value) {
                    $body = str_replace('{var_' . $key . '}', $value, $body);
                }
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
                    // content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                if ($emm) {
                    $this->session->set_flashdata('messagePr', 'Your password reset link has been sent to your email address');
                    redirect(base_url() . 'user/login', 'refresh');
                }
            } else {
                $this->session->set_flashdata('forgotpassword', 'This account does not exist'); //die;
                redirect(base_url() . "user/forgetpassword");
            }
        } else {
            $this->load->view('include/script');
            $this->load->view('forget_password');
        }
    }

    /**
     * This function is used to load view of reset password and varify user too 
     * @return : void
     */
    public function mail_varify() {
        $return = $this->User_model->mail_varify();
        $this->load->view('include/script');
        if ($return) {
            $data['email'] = $return;
            $this->load->view('set_password', $data);
        } else {
            $data['email'] = 'allredyUsed';
            $this->load->view('set_password', $data);
        }
    }

    /**
     * This function is used to reset password in forget password process
     * @return : void
     */
    public function reset_password() {
        $return = $this->User_model->ResetPpassword();
        if ($return) {
            $this->session->set_flashdata('messagePr', 'Password Changed Successfully..');
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            $this->session->set_flashdata('messagePr', 'Unable to update password');
            redirect(base_url() . 'user/login', 'refresh');
        }
    }

    /**
     * This function is generate hash code for random string
     * @return string
     */
    public function getVarificationCode() {
        $pw = $this->randomString();
        return $varificat_key = password_hash($pw, PASSWORD_DEFAULT);
    }

    /**
     * This function is used for show users list
     * @return Void
     */
    public function userTable() {
        is_login();
        
        $CI = get_instance();
        $data = array();
        $user_type = $CI->session->get_userdata()['user_details'][0]->user_type;
        if($user_type == 'Manager' || $user_type == 'admin'){
            $data['user_type'] = $user_type;
        }else{
            $user_type = '';
        }
                
        if (($user_type == 'admin' || $user_type == 'Manager') && CheckPermission("users", "own_read")) {
            $this->load->view('include/header');
            $this->load->view('user_table', $data);
            $this->load->view('include/footer');
        } else {
            $this->session->set_flashdata('messagePr', 'You don\'t have permission to access.');
            redirect(base_url() . 'user/profile', 'refresh');
        }
    }

    /**
     * This function is used to create datatable in onboarding users list page
     * @return Void
     */
    public function dataTable() {
        is_login();
        $table = 'users'; //calling table name for permissions
        $primaryKey = 'users_id';
        $columns = array(
            array('db' => 'users_id', 'dt' => 0),
            array('db' => 'status', 'dt' => 1),            
            array('db' => 'name', 'dt' => 2),
            array('db' => 'email', 'dt' => 3),
            array('db' => 'created_on', 'dt' => 5),
            array('db' => 'user_type', 'dt' => 6)
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $where = array("stage_status IN ('Onboarding', 'Verified')");
        $output_arr = SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where);

        foreach ($output_arr['data'] as $key => $value) {

            $id = $output_arr['data'][$key][0];
            $status = $output_arr['data'][$key][1];
            $username = $output_arr['data'][$key][2];
            $useremail = $output_arr['data'][$key][3];
            $created_on = $output_arr['data'][$key][5];
            $user_type = $output_arr['data'][$key][6];
            
            $output_arr['data'][$key][0] = '<input type="checkbox" name="selData" value="' . $id . '">';
            $output_arr['data'][$key][1] = $status;
            $output_arr['data'][$key][2] = $user_type;
            $output_arr['data'][$key][3] = $username;
            $output_arr['data'][$key][4] = $useremail;
            $output_arr['data'][$key][5] = '';
            if (CheckPermission($table, "all_update")) {
                $output_arr['data'][$key][5] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="fa fa-pencil" data-id=""></i></a>';
            } else if (CheckPermission($table, "own_update") && (CheckPermission($table, "all_update") != true)) {
                $user_id = getRowByTableColomId($table, $id, 'users_id', 'user_id');
                if ($user_id == $this->user_id) {
                    $output_arr['data'][$key][5] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="fa fa-pencil" data-id=""></i></a>';
                }
            }

            if (CheckPermission($table, "all_delete")) {
                $output_arr['data'][$key][5] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="fa fa-trash-o" ></i></a>';
            } else if (CheckPermission($table, "own_delete") && (CheckPermission($table, "all_delete") != true)) {
                $user_id = getRowByTableColomId($table, $id, 'users_id', 'user_id');
                if ($user_id == $this->user_id) {
                    $output_arr['data'][$key][5] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="fa fa-trash-o" ></i></a>';
                }
            }            

            $output_arr['data'][$key][6] = $created_on;            
        }

        echo json_encode($output_arr);
    }

    /**
     * This function is used to create datatable in users list page registered user list
     * @return Void
     */
    public function dataTableForRegisteredUsers() {
        is_login();
        $table = 'users'; //calling table name for permissions
        $primaryKey = 'users_id';
        $columns = array(
            array('db' => 'users_id', 'dt' => 0),
            array('db' => 'status', 'dt' => 1),            
            array('db' => 'name', 'dt' => 2),
            array('db' => 'email', 'dt' => 3),
            array('db' => 'created_on', 'dt' => 5),
            array('db' => 'user_type', 'dt' => 6)
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $where = array("stage_status IN ('Registered')");
        $output_arr = SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where);

        foreach ($output_arr['data'] as $key => $value) {

            $id = $output_arr['data'][$key][0];
            $status = $output_arr['data'][$key][1];
            $username = $output_arr['data'][$key][2];
            $useremail = $output_arr['data'][$key][3];
            $created_on = $output_arr['data'][$key][5];
            $user_type = $output_arr['data'][$key][6];
            
            $output_arr['data'][$key][0] = '<input type="checkbox" name="selData" value="' . $id . '">';
            $output_arr['data'][$key][1] = $status;
            $output_arr['data'][$key][2] = $user_type;
            $output_arr['data'][$key][3] = $username;
            $output_arr['data'][$key][4] = $useremail;
            $output_arr['data'][$key][5] = '';
            if (CheckPermission($table, "all_update")) {
                $output_arr['data'][$key][5] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="fa fa-pencil" data-id=""></i></a>';
            } else if (CheckPermission($table, "own_update") && (CheckPermission($table, "all_update") != true)) {
                $user_id = getRowByTableColomId($table, $id, 'users_id', 'user_id');
                if ($user_id == $this->user_id) {
                    $output_arr['data'][$key][5] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="fa fa-pencil" data-id=""></i></a>';
                }
            }

            if (CheckPermission($table, "all_delete")) {
                $output_arr['data'][$key][5] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="fa fa-trash-o" ></i></a>';
            } else if (CheckPermission($table, "own_delete") && (CheckPermission($table, "all_delete") != true)) {
                $user_id = getRowByTableColomId($table, $id, 'users_id', 'user_id');
                if ($user_id == $this->user_id) {
                    $output_arr['data'][$key][5] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="fa fa-trash-o" ></i></a>';
                }
            }            

            $output_arr['data'][$key][6] = $created_on;            
        }

        echo json_encode($output_arr);
    }

    /**
     * This function is used to create datatable in users list page invited user list
     * @return Void
     */
    public function dataTableForInvitedUsers() {
        is_login();
        $table = 'users'; //calling table name for permissions
        $primaryKey = 'users_id';
        $columns = array(
            array('db' => 'users_id', 'dt' => 0),
            array('db' => 'status', 'dt' => 1),            
            array('db' => 'name', 'dt' => 2),
            array('db' => 'email', 'dt' => 3),
            array('db' => 'created_on', 'dt' => 5),
            array('db' => 'user_type', 'dt' => 6)
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $where = array("stage_status IN ('Invited', 'Reinvited')");
        $output_arr = SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where);

        foreach ($output_arr['data'] as $key => $value) {

            $id = $output_arr['data'][$key][0];
            $status = $output_arr['data'][$key][1];
            $username = $output_arr['data'][$key][2];
            $useremail = $output_arr['data'][$key][3];
            $created_on = $output_arr['data'][$key][5];
            $user_type = $output_arr['data'][$key][6];
            
            $output_arr['data'][$key][0] = '<input type="checkbox" name="selData" value="' . $id . '">';
            $output_arr['data'][$key][1] = $status;
            $output_arr['data'][$key][2] = $user_type;
            $output_arr['data'][$key][3] = $username;
            $output_arr['data'][$key][4] = $useremail;
            $output_arr['data'][$key][5] = '';
            if (CheckPermission($table, "all_update")) {
                $output_arr['data'][$key][5] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="fa fa-pencil" data-id=""></i></a>';
            } else if (CheckPermission($table, "own_update") && (CheckPermission($table, "all_update") != true)) {
                $user_id = getRowByTableColomId($table, $id, 'users_id', 'user_id');
                if ($user_id == $this->user_id) {
                    $output_arr['data'][$key][5] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="fa fa-pencil" data-id=""></i></a>';
                }
            }

            if (CheckPermission($table, "all_delete")) {
                $output_arr['data'][$key][5] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="fa fa-trash-o" ></i></a>';
            } else if (CheckPermission($table, "own_delete") && (CheckPermission($table, "all_delete") != true)) {
                $user_id = getRowByTableColomId($table, $id, 'users_id', 'user_id');
                if ($user_id == $this->user_id) {
                    $output_arr['data'][$key][5] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="fa fa-trash-o" ></i></a>';
                }
            }            

            $output_arr['data'][$key][6] = $created_on;            
        }

        echo json_encode($output_arr);
    }

    /**
     * This function is Showing users profile
     * @return Void
     */
    public function profile($id = '') {

        is_login();
        if (!isset($id) || $id == '') {

            $id = $this->session->userdata('user_details')[0]->users_id;
        }
        $data['user_data'] = $this->User_model->get_users($id);
        $this->load->view('include/header');
        $this->load->view('profile', $data);
        $this->load->view('include/footer');
    }

    /**
     * This function is used to show popup of user to add and update
     * @return Void
     */
    public function get_modal() {
        is_login();
        if ($this->input->post('id')) {
            $data['userData'] = getDataByid('users', $this->input->post('id'), 'users_id');
            echo $this->load->view('add_user', $data, true);
        } else {
            echo $this->load->view('add_user', '', true);
        }
        exit;
    }

    /**
     * This function is used to upload file
     * @return Void
     */
    function upload() {
        foreach ($_FILES as $name => $fileInfo) {
            $filename = $_FILES[$name]['name'];
            $tmpname = $_FILES[$name]['tmp_name'];
            $exp = explode('.', $filename);
            $ext = end($exp);
            $newname = $exp[0] . '_' . time() . "." . $ext;
            $config['upload_path'] = 'assets/images/';
            $config['upload_url'] = base_url() . 'assets/images/';
            $config['allowed_types'] = "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp";
            $config['max_size'] = '2000000';
            $config['file_name'] = $newname;
            $this->load->library('upload', $config);
            move_uploaded_file($tmpname, "assets/images/" . $newname);
            return $newname;
        }
    }

    /**
     * This function is used to add and update users
     * @return Void
     */
    public function add_edit($id = '') {
        $data = $this->input->post();
        $profile_pic = 'user.png';
        if ($this->input->post('users_id')) {
            $id = $this->input->post('users_id');
        }
        
        if (isset($this->session->userdata('user_details')[0]->users_id)) {
            if ($this->input->post('users_id') == $this->session->userdata('user_details')[0]->users_id) {
                $redirect = 'profile';
            } else {
                $redirect = 'userTable';
            }
        } else {            
            //$this->session->set_flashdata('messagePr', 'You email address is already registered with us.');
            $redirect = 'login';
            //redirect(base_url() . 'user/' . $redirect, 'refresh');
        }
        if ($this->input->post('fileOld')) {
            $newname = $this->input->post('fileOld');
            $profile_pic = $newname;
        } else {
            $data[$name] = '';
            $profile_pic = 'user.png';
        }
        foreach ($_FILES as $name => $fileInfo) {
            if (!empty($_FILES[$name]['name'])) {
                $newname = $this->upload();
                $data[$name] = $newname;
                $profile_pic = $newname;
            } else {
                if ($this->input->post('fileOld')) {
                    $newname = $this->input->post('fileOld');
                    $data[$name] = $newname;
                    $profile_pic = $newname;
                } else {
                    $data[$name] = '';
                    $profile_pic = 'user.png';
                }
            }
        }
        if ($id != '') {
            $data = $this->input->post();
            if ($this->input->post('status') != '') {
                $data['status'] = $this->input->post('status');
            }
            if ($this->input->post('users_id') == 1) {
                $data['user_type'] = 'admin';
            }
            if ($this->input->post('password') != '') {
                if ($this->input->post('currentpassword') != '') {
                    $old_row = getDataByid('users', $this->input->post('users_id'), 'users_id');
                    if (password_verify($this->input->post('currentpassword'), $old_row->password)) {
                        if ($this->input->post('password') == $this->input->post('confirmPassword')) {
                            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                            $data['password'] = $password;
                        } else {
                            $this->session->set_flashdata('messagePr', 'Password and confirm password should be same...');
                            redirect(base_url() . 'user/' . $redirect, 'refresh');
                        }
                    } else {
                        $this->session->set_flashdata('messagePr', 'Enter Valid Current Password...');
                        redirect(base_url() . 'user/' . $redirect, 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('messagePr', 'Current password is required');
                    redirect(base_url() . 'user/' . $redirect, 'refresh');
                }
            }
            $id = $this->input->post('users_id');
            unset($data['fileOld']);
            unset($data['currentpassword']);
            unset($data['confirmPassword']);
            unset($data['users_id']);
            unset($data['employee_id']);
            //unset($data['user_type']);

            if($this->input->post('employee_id') != ''){
                $data['employee_id'] = $this->input->post('employee_id');
            }

            $data['user_type'] = $this->input->post('user_type');
            
            if (isset($data['edit'])) {
                unset($data['edit']);
            }
            if ($data['password'] == '') {
                unset($data['password']);
            }
            $data['profile_pic'] = $profile_pic;
            $data['updated_by'] = $this->session->userdata('user_details')[0]->users_id;
            
            $res = $this->User_model->updateRow('users', 'users_id', $id, $data);
            if($res){
                $this->session->set_flashdata('messagePr', 'Your data updated Successfully..');
            }
            
            redirect(base_url() . 'user/' . $redirect, 'refresh');
            
        } else {
            
            if ($this->input->post('user_type') != 'admin') {
                $data = $this->input->post();
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $checkValue = $this->User_model->check_exists('users', 'email', $this->input->post('email'));
                                                
                if ($checkValue == false) {
                                        
                    $this->session->set_flashdata('messagePr', 'This Email Already Registered with us..');
                    redirect(base_url() . 'user/userTable', 'refresh');
                }
                $checkValue1 = $this->User_model->check_exists('users', 'name', $this->input->post('name'));
                if ($checkValue1 == false) {
                    $this->session->set_flashdata('messagePr', 'Username Already Registered with us..');
                    redirect(base_url() . 'user/userTable', 'refresh');
                }
                $data['status'] = 'active';
                if (setting_all('admin_approval') == 1) {
                    $data['status'] = 'deleted';
                }
                
                if ($this->input->post('status') != '') {
                    $data['status'] = $this->input->post('status');
                }
                //$data['token'] = $this->generate_token();
                $data['user_id'] = $this->user_id;
                $data['password'] = $password;
                $data['profile_pic'] = $profile_pic;
                $data['is_deleted'] = 0;
                if (isset($data['password_confirmation'])) {
                    unset($data['password_confirmation']);
                }
                if (isset($data['call_from'])) {
                    unset($data['call_from']);
                }
                unset($data['submit']);
                
                $data['created_by'] = $this->session->userdata('user_details')[0]->users_id;
                $data['updated_by'] = $this->session->userdata('user_details')[0]->users_id;
                $this->User_model->insertRow('users', $data);
                redirect(base_url() . 'user/' . $redirect, 'refresh');
            } else {
                $this->session->set_flashdata('messagePr', 'You Don\'t have this autherity ');
                redirect(base_url() . 'user/registration', 'refresh');
            }
        }
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function delete($id) {
        is_login();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->delete($id);
        }
        redirect(base_url() . 'user/userTable', 'refresh');
    }

    /**
     * This function is used to send invitation mail to users for registration
     * @return Void
     */
    public function InvitePeople() {
        is_login();
        
        if ($this->input->post('emails')) {
            $setting = settings();
            $var_key = $this->randomString();
            $emailArray = explode(',', $this->input->post('emails'));
            $emailArray = array_map('trim', $emailArray);
            
            $body = $this->User_model->get_template('invitation');
            $subject_line = 'User Invitation';
            
            $result['existCount'] = 0;
            $result['successCount'] = 0;
            $result['invalidEmailCount'] = 0;
            $result['noTemplate'] = 0;
            if (isset($body->html) && $body->html != '') {
                $body = $body->html;
                foreach ($emailArray as $mailKey => $mailValue) {
                    if (filter_var($mailValue, FILTER_VALIDATE_EMAIL)) {
                        $res = $this->User_model->get_data_by('users', $mailValue, 'email');
                        
                        if (is_array($res) && empty($res)) {

                            $link = (string) '<a href="' . base_url() . 'user/registration?invited=' . $var_key . '">Click here</a> to register.';
                            $uname = trim(ucfirst($this->input->post('user_name_by_admin')));
                                                        
                            // $remail = $this->input->post('recruiter_email');                            
                            $recruiter_email = $this->session->userdata('user_details')[0]->email;
                            $remail = array("shahaji9@gmail.com", $recruiter_email);
                            //$remail = array($recruiter_email);
                            //$remail = array("shahaji9@gmail.com"); //Added all mails as Cc
                            $offer_letter = $this->input->post('offer_letter');
                            
                            $data = array('var_user_email' => $mailValue, 'var_invitation_link' => $link, 'var_user_name' => $uname);
                            foreach ($data as $key => $value) {
                                $body = str_replace('{' . $key . '}', $value, $body);
                            }

                            if ($setting['mail_setting'] == 'php_mailer') {
                                $this->load->library("send_mail"); 
                                //validate form field for attaching the file 
                                $emm = $this->send_mail->email($subject_line . ' | Gift Group', $body, $mailValue, $setting, $remail);
                                                                
                            } else {
                            
                                // content-type is required when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";

                                $emm = mail($mailValue, $subject_line . ' | Gift Group', $body, $headers);
                            }
                            if ($emm) {
                                $darr = array('email' => $mailValue, 'var_key' => $var_key, 'user_name_by_admin' => $uname, 'recruiter_email' => $recruiter_email);
                                
                                $session_user_id = $this->session->userdata('user_details')[0]->users_id;
                                $darr['created_by'] = $session_user_id;
                                $darr['updated_by'] = $session_user_id;

                                $this->User_model->insertRow('users', $darr);
                                $result['successCount'] += 1;
                            }
                        } else {
                            $result['existCount'] += 1;
                        }
                    } else {
                        $result['invalidEmailCount'] += 1;
                    }
                }
            } else {
                $result['noTemplate'] = 'No Email Template Available.';
            }
        }
        echo json_encode($result);
        exit;
    }
    
    /**
     * This function is used to Check invitation code for user registration
     * @return TRUE/FALSE
     */
    public function chekInvitation() {
        if ($this->input->post('code') && $this->input->post('code') != '') {
            $res = $this->User_model->get_data_by('users', $this->input->post('code'), 'var_key');
            $result = array();
            if (is_array($res) && !empty($res)) {
                $result['email'] = $res[0]->email;
                $result['users_id'] = $res[0]->users_id;
                $result['result'] = 'success';
            } else {
                $this->session->set_flashdata('messagePr', 'This code is not valid..');
                $result['result'] = 'error';
            }
        }
        echo json_encode($result);
        exit;
    }
    
    /**
     * This function is used to Check invitation code for user check invitation for registration
     * @return TRUE/FALSE
     */
    public function checkInvitationForRegistration($email) {
        
        if (!empty($email)) {
            $res = $this->User_model->get_data_by('users', $email, 'email');
            $result = array();
            if (is_array($res) && !empty($res)) {
                redirect(base_url() . 'user/registration?invited='. $res[0]->var_key, 'refresh');             
            } else {
                return FALSE;
            }
        }
        
        return FALSE;
    }

    /**
     * This function is used to register invited user
     * @return Void
     */
    public function register_invited($id) {
        $data = $this->input->post();
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $data['password'] = $password;
        $data['var_key'] = NULL;
        $data['is_deleted'] = 0;
        $data['status'] = 'active';
        $data['user_id'] = 1;
        $data['stage_status'] = 'Registered';
        if (isset($data['password_confirmation'])) {
            unset($data['password_confirmation']);
        }
        if (isset($data['call_from'])) {
            unset($data['call_from']);
        }
        if (isset($data['submit'])) {
            unset($data['submit']);
        }
        
        $data['updated_by'] = $this->session->userdata('user_details')[0]->users_id;
        
        $this->User_model->updateRow('users', 'users_id', $id, $data);
        $this->session->set_flashdata('messagePr', 'Successfully Registered..');
        redirect(base_url() . 'user/login', 'refresh');
    }

    /**
     * This function is used to check email is alredy exist or not
     * @return TRUE/FALSE
     */
    public function checEmailExist() {
        $result = 1;
        $res = $this->User_model->get_data_by('users', $this->input->post('email'), 'email');
        if (!empty($res)) {
            if ($res[0]->users_id != $this->input->post('uId')) {
                $result = 0;
            }
        }
        echo $result;
        exit;
    }

    /**
     * This function is used to Generate a token for varification
     * @return String
     */
    public function generate_token() {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric;
        $token = '';
        $up_lp_char = $alpha . $alpha_upper . $special;
        $chars = str_shuffle($chars);
        $token = substr($chars, 10, 10) . strtotime("now") . substr($up_lp_char, 8, 8);
        return $token;
    }

    /**
     * This function is used to Generate a random string
     * @return String
     */
    public function randomString() {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric;
        $pw = '';
        $chars = str_shuffle($chars);
        $pw = substr($chars, 8, 8);
        return $pw;
    }

    public function verifyUserDocuments() {

        $data = $this->input->get();        
        $data['verified_by'] = $this->session->userdata('user_details')[0]->users_id;
        $data['stage_status'] = 'Verified';
                
        $this->User_model->updateRow('users', 'users_id', $data['users_id'], $data);

        $this->session->set_flashdata('messagePr', 'User Documents Verified Successfully.');
        redirect(base_url() . 'user/userTable', 'refresh');
    }

    public function createZipFolder() {

        $data = $this->input->get();

        $this->load->library('zip');
        $this->load->helper('download');

        $dir = FCPATH . "assets/documents/" . $data['users_id'] . "/";        
        $userData = $this->User_model->get_users($data['users_id']);
                
        foreach (glob($dir . '*') as $file) {
            
            if($userData[0]->employee_id){
                $newFilename = preg_split('/\//', $file);
                $newFilename1 = $userData[0]->employee_id. '-'. end($newFilename);
                $this->zip->read_file($file, $newFilename1);
            } else {
                $newFilename1 = $file;
                $this->zip->read_file($file);
            }
            
            //$this->zip->read_file($file, $newFilename1);
        }
        
        $filename = trim(preg_replace('/\s+/', '_', $data['users_name'])) . '_' . $data['users_id'] . '.zip';                
        $this->zip->download($filename);

        echo $filename;
    }  

    public function employee_onboarding_details() {

        $data_get = $this->input->get();
        $username = $data_get['users_name'];
        $users_id = $data_get['users_id'];
        
        $data = [];
        $data['db_data'] = $this->User_model->getUserDetailsForPDFFinal($users_id);
        //load the view and saved it into $html variable
        $html = $this->load->view('onboarding-form-create-pdf', $data, true);
        
        if(empty($data_get['users_name']) && empty($data_get['users_id'])){
            $username = "Onboarding User Details";
            $users_id = 1;
        }
        
        // create file name
        $pdfFileName = trim(preg_replace('/\s+/', '_', $username)) . '_' . $users_id . '.pdf';
        
        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFileName, "D");
    }
}