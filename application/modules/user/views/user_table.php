<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata("messagePr")) { ?>
            <div class="alert alert-info">      
                <?php echo $this->session->flashdata("messagePr") ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata("error")) { ?>
            <div class="alert alert-danger">      
                <?php echo $this->session->flashdata("error") ?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success users-list-wrapper">
                    <div class="box-header with-border">
                        <h3 class="box-title">User</h3>
                        <div class="box-tools">
                            <?php if (CheckPermission("users", "own_create")) { ?>
                                <!--<button type="button" class="btn-sm btn btn-success modalButtonUser" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Add User</button>-->
                            <?php } if (setting_all('email_invitation') == 1) { ?>
                                <button type="button" class="btn-sm btn btn-success InviteUser" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Send Invitation</button>
                            <?php } ?>
                        </div>
                    </div>                    
                    <!-- /.box-header -->
                    
                    <h4>Users List</h4>
                    <div class="box-body">
						<div id="filter-by-joining-date">
                            <div class="pull-right">
                                <label>Search all: </label>
                                <input type="text" id="search-all" name="search-all" value="" placeholder="Global Search" />
                            </div>					
						</div>                     
                        <table id="example1" class="cell-border example1 table table-striped table1 delSelTable">
                            <thead>
                                <tr>
                                    <th width="5%"><input type="checkbox" class="selAll"></th>                                    
                                    <th width="10%">Status</th>
                                    <th width="10%">User Type</th>
                                    <th width="25%">Name</th>
                                    <th width="25%">Email</th>    
                                    <th width="10%">Action</th>
                                    <th width="15%">Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody> 
                        </table>
                    </div>
                    <!-- /.box-body -->

                    <h4>Registered Users List</h4>
                    <div class="box-body">   
                        <table id="registered-users-list" class="cell-border registered-users-list table table-striped table1 delSelTable">
                            <thead>
                                <tr>
                                    <th width="5%"><input type="checkbox" class="selAll"></th>                                    
                                    <th width="10%">Status</th>
                                    <th width="10%">User Type</th>
                                    <th width="25%">Name</th>
                                    <th width="25%">Email</th>    
                                    <th width="10%">Action</th>
                                    <th width="15%">Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody> 
                        </table>
                    </div>
                    <!-- /.box-body -->
                    
                    <h4>Invited Users List</h4>
                    <div class="box-body">   
                        <table id="invited-users" class="cell-border invited-users table table-striped table1 delSelTable">
                            <thead>
                                <tr>
                                    <th width="5%"><input type="checkbox" class="selAll"></th>                                    
                                    <th width="10%">Status</th>
                                    <th width="10%">User Type</th>
                                    <th width="25%">Name</th>
                                    <th width="25%">Email</th>    
                                    <th width="10%">Action</th>
                                    <th width="15%">Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody> 
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>  
<!-- Modal Crud Start-->
<div class="modal fade" id="nameModal_user" role="dialog">
    <div class="modal-dialog">
        <div class="box box-primary popup" >
            <div class="box-header with-border formsize">
                <h3 class="box-title">User Form</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <!-- /.box-header -->
            <div class="modal-body" style="padding: 0px 0px 0px 0px;"></div>
        </div>
    </div>
</div><!--End Modal Crud --> 