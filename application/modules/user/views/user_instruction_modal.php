<style>
#basic-instructions .modal-footer,
#basic-instructions .modal-title{
    text-align: center;
}
#basic-instructions ul{
    padding-left: 0;
    list-style: circle;
}
#basic-instructions ul ul{
    padding-left: 15px;
}
#basic-instructions .basic-instructions{
    font-size: 16px;
    line-height: 1.8em;
}
#basic-instructions .dev-note{
    color: #da0303;
}
</style>
<div id="basic-instructions" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-md-12">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Instructions for the Onboarding form</h3>
            </div>
            <div class="modal-body">
                <div class="basic-instructions">
                    <ul>					
                        <li>Make sure you open the link in <strong>‘Google Chrome’</strong> or <strong>‘Firefox’</strong></li>
                        <li>Keep the following scanned documents handy before registering:
                            <ul>
                                <li>Aadhar card (both sides)</li>
                                <li>Pan card</li>
                                <li>Salary slips (last 3 months)</li>
                                <li>Bank statements of last 3 months (Optional)</li>
                                <li>Latest salary revision letter</li>
                                <li>Experience letters (last 3 years)</li>
                                <li>All educational and professional qualification documents</li>
                                <li>Digital/Soft copy of your photo</li>
                            </ul>
                        </li>
                    </ul>
                    <p class="dev-note">Note: If you are facing any issue while switching to next step then, Please try to logout and login again.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
    $(window).on('load',function(){
        $('#basic-instructions').modal('show');
    });
</script>