<?php
class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->user_id = isset($this->session->get_userdata()['user_details'][0]->id) ? $this->session->get_userdata()['user_details'][0]->users_id : '1';
        $this->load->model('onboarding/Onboarding_model');
    }

    /**
     * This function is used authenticate user at login
     */
    function auth_user() {

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where("is_deleted='0' AND (name='$email' OR email='$email')");
        $result = $this->db->get('users')->result();
                
        $this->db->where('users_id', $result[0]->users_id);
        $this->db->where('is_active', TRUE);
        
        if (!empty($result)) {
            if (password_verify($password, $result[0]->password)) {
                if ($result[0]->status != 'active') {
                    return 'not_varified';
                }
                
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete user
     * @param: $id - id of user table
     */
    function delete($id = '') {
        $this->db->where('users_id', $id);
        $this->db->delete('users');
    }

    /**
     * This function is used to load view of reset password and varify user too 
     */
    function mail_varify() {
        $ucode = $this->input->get('code');
        $this->db->select('email as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            return $result->e_mail;
        } else {
            return false;
        }
    }

    /**
     * This function is used Reset password  
     */
    function ResetPpassword() {
        $email = $this->input->post('email');
        if ($this->input->post('password_confirmation') == $this->input->post('password')) {
            $npass = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $data['password'] = $npass;
            $data['var_key'] = '';
            return $this->db->update('users', $data, "email = '$email'");
        }
    }

    /**
     * This function is used to select data form table  
     */
    function get_data_by($tableName = '', $value = '', $colum = '', $condition = '') {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * This function is used to check user is alredy exist or not  
     */
    function check_exists($table = '', $colom = '', $colomValue = '') {
        $this->db->where($colom, $colomValue);
        $res = $this->db->get($table)->row();
        
        if (!empty($res)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * This function is used to get users detail  
     */
    function get_users($userID = '') {
        $this->db->where('is_deleted', '0');
        if (isset($userID) && $userID != '') {
            $this->db->where('users_id', $userID);
        } else if ($this->session->userdata('user_details')[0]->user_type == 'admin') {
            $this->db->where('user_type', 'admin');
        } else {
            $this->db->where('users.users_id !=', '1');
        }
        $this->db->order_by('updated_on', 'DESC');
        $result = $this->db->get('users')->result(); 
        
        return $result;
    }

    /***
     * Get user name by user id
     * ** */
    function get_user_info_by_id($userID = '', $createdBy = '') {

        $newResult = array();
        if (isset($userID) && $userID != '') {
            $this->db->where('users_id', $userID);
            $this->db->where('is_active', TRUE);

            $result_revised_offer_info = $this->db->get('revised_offer_details')->result_array();
            
            $this->db->where("users_id", $userID);
            $result = $this->db->get('users')->result();

            $this->db->where('users_id', $userID);
            $result_onboarding = $this->db->get('onboarding')->result();

            if(count($result_revised_offer_info) > 0){
                $newResult['date_of_joining_by_hr'] = $result_revised_offer_info[0]['date_of_joining_by_hr'];
            } else if (count($result_onboarding) > 0){
                $newResult['date_of_joining_by_hr'] = $result_onboarding[0]->date_of_joining;
            } else {
                $newResult['date_of_joining_by_hr'] = $result[0]->date_of_joining_by_hr;
            }
        }        
        
        if (isset($createdBy) && $createdBy != '') {
            $this->db->where('users_id', $createdBy);
        }
        $result_created_by = $this->db->get('users')->result();
        $newResult['offered_by'] = $result_created_by[0]->name;

        return $newResult;
    }

    /**
     * This function is used to get email template  
     */
    function get_template($code) {
        $this->db->where('code', $code);
        return $this->db->get('templates')->row();
    }

    /**
     * This function is used to Insert record in table  
     */
    public function insertRow($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * This function is used to Update record in table  
     */
    public function updateRow($table, $col, $colVal, $data) {
        
        $this->db->where($col, $colVal);
        $this->db->update($table, $data);
        $db_error = $this->db->error();
        if (!empty($db_error)) {
            
            if($db_error['code'] != 0){
                $this->session->set_flashdata('error', $db_error['message']);
                return false;
            }
        }
        
        return true;
    }

    public function insert_or_update_revised_offer_details($users_id, $data) {

        if(isset($data)){
            $newValue = array();
            $this->db->where("users_id", $users_id);
            $this->db->where("is_active", TRUE);
            
            $newValue["updated_by"] = $data['updated_by'];
            $newValue["is_active"] = FALSE;
            
            $this->db->update("revised_offer_details", $newValue);
            $update_status = $this->db->affected_rows();
            
            $newValue2 = array();
            $newValue2["users_id"] = $users_id;
            if($data['offer_letter_file_name'] != ''){
                $newValue2["offer_letter_file_name"] = $data['offer_letter_file_name'];
            }
            
            $newValue2["date_of_joining_by_hr"] = $data['date_of_joining_by_hr'];
            $newValue2["created_by"] = $data['created_by'];
            $newValue2["updated_by"] = $data['updated_by'];
            $newValue2["is_active"] = TRUE;
            
            $insertSuccess = $this->db->insert("revised_offer_details", $newValue2);
            
            if($insertSuccess){
                $update_doj = array();
                $update_doj['date_of_joining'] = $data['date_of_joining_by_hr'];
                $update_query = $this->Onboarding_model->insert_or_update_onboarding_step1($users_id, $update_doj);
                
                return true;
            }      
        } else {
            return false;
        }
    }

    public function getUsersDetailsExport($users_id) {

        $this->db->select('u.users_id, u.name, u.email, CONCAT(o.employee_fname, " ", o.employee_mname, " ", o.employee_lname) as employee_name,
						CONCAT(o.father_fname, " ", o.father_mname, " ", o.father_lname) as father_name,
						CONCAT(o.spouse_fname, " ", o.spouse_mname, " ", o.spouse_lname) as spouse_name,
						o.children_count');
        $this->db->from('users as u');
        $this->db->join('onboarding as o', 'u.users_id = o.users_id', 'INNER');
        $this->db->where('u.status', 'active');
        $this->db->where('u.user_type', 'Member');
        $this->db->where('o.is_active', 1);
        $this->db->where('u.users_id', $users_id);
        
        $result = $this->db->get()->result_array();
        
        return $result;
    }

    //SELECT `u`.`users_id`, `u`.`name`, `u`.`email`, CONCAT(o.employee_fname, " ", `o`.`employee_mname`, " ", o.employee_lname) as employee_name, CONCAT(o.father_fname, " ", `o`.`father_mname`, " ", o.father_lname) as father_name, CONCAT(o.spouse_fname, " ", `o`.`spouse_mname`, " ", o.spouse_lname) as spouse_name, `o`.`children_count` FROM `users` as `u` INNER JOIN `onboarding` as `o` ON `u`.`users_id` = `o`.`users_id` WHERE `u`.`status` = 'active' AND `u`.`user_type` = 'Member' AND `o`.`is_active` = 1 AND `u`.`users_id` = 6
    public function getUsersDetailsForPDF($users_id) {

        $this->db->select('u.users_id, u.name, u.email, u.employee_id, u.profile_pic, CONCAT(o.employee_fname, " ", o.employee_mname, " ", o.employee_lname) as employee_name,
                                                o.employee_fname, o.employee_mname, o.employee_lname,
						CONCAT(o.father_fname, " ", o.father_mname, " ", o.father_lname) as father_name,
						CONCAT(o.spouse_fname, " ", o.spouse_mname, " ", o.spouse_lname) as spouse_name,
                                                DATE_FORMAT(o.date_of_birth, "%d-%b-%Y") as date_of_birth, o.aadhaar_number, o.pan_number, o.pf_number_exist, o.pf_number, o.uan_number, o.gender, o.hobbies, o.age, o.blood_group, o.contact_no1, o.contact_no2, o.email_id,
                                                o.contact_address, o.contact_city, o.contact_state, o.contact_pin_code, o.permanent_address, o.permanent_city, o.permanent_state, o.permanent_pin_code, CONCAT(o.ice_fname," ",o.ice_mname," ",o.ice_lname) as ice_name, o.ice_contact_no,
                                                o.ice_relationship, o.passport_number, o.passport_issue_date, o.passport_expiry_date,
                                                o.marital_status, o.nationality, DATE_FORMAT(o.date_of_joining, "%d-%b-%Y") as date_of_joining,
                                                (select experience_organization from onboarding_experience where users_id='. $users_id .' order by experience_period_to_year desc limit 1) as experience_organization,
                                                (select experience_designation from onboarding_experience where users_id='. $users_id .' order by experience_period_to_year desc limit 1 ) as experience_designation,
                                                (select experience_gross_salary_month from onboarding_experience where users_id='. $users_id .' order by experience_period_to_year desc limit 1) as experience_gross_salary_month,
                                                (select sum(total_experience) as total from onboarding_experience where users_id='. $users_id .') as total_experience,
                                                o.children_count, DATE_FORMAT(o.updated_on, "%d-%m-%Y %h:%i:%s %p") as updated_on');
        $this->db->from('users as u');
        $this->db->join('onboarding as o', 'u.users_id = o.users_id', 'INNER');
        $this->db->where('u.status', 'active');
        $this->db->where_in('u.user_type', array('Member', 'Manager'));
        $this->db->where('o.is_active', 1);
        $this->db->where('u.users_id', $users_id);
        
        $result = $this->db->get()->result_array();
                        
        //print_r($this->db->last_query());
        return $result;
    }
    
    // select reference_name,reference_organization_institution, reference_designation, reference_phone, reference_email 
    // from onboarding_references where users_id = 1 and is_active = 1;

    public function getUsersDetailsReferencesForPDF($users_id) {

        $this->db->select('reference_name,reference_organization_institution, reference_designation, reference_phone, reference_email');
        $this->db->from('onboarding_references');
        $this->db->where('users_id', $users_id);
        $this->db->where('is_active', 1);

        $result = $this->db->get()->result_array();
        
        return $result;
    }

    // select edu_qualification, edu_qualification_other, edu_qualification_university, edu_qualification_passing_year, edu_qualification_percentage_marks,
    // edu_qualification_specialization from onboarding_educational_qualification where users_id = 6 and is_active = 1 order by edu_qualification_passing_year desc;

    public function getUsersDetailsEduQualificationForPDF($users_id) {

        $this->db->select('edu_qualification, edu_qualification_other, edu_qualification_university, edu_qualification_passing_year, 
							edu_qualification_percentage_marks,edu_qualification_specialization');
        $this->db->from('onboarding_educational_qualification');
        $this->db->where('users_id', $users_id);
        $this->db->where('is_active', 1);
        $this->db->order_by("edu_qualification_passing_year", "desc");

        $result = $this->db->get()->result_array();

        return $result;
    }

    //select pro_qualification, pro_qualification_university, pro_qualification_passing_year, pro_qualification_percentage_marks,
    // pro_qualification_specialization from onboarding_professional_qualification where users_id = 6 and is_active = 1 order by pro_qualification_passing_year desc;

    public function getUsersDetailsProQualificationForPDF($users_id) {

        $this->db->select('pro_qualification, pro_qualification_university, pro_qualification_passing_year, pro_qualification_percentage_marks, 
							pro_qualification_specialization');
        $this->db->from('onboarding_professional_qualification');
        $this->db->where('users_id', $users_id);
        $this->db->where('is_active', 1);
        $this->db->order_by("pro_qualification_passing_year", "desc");

        $result = $this->db->get()->result_array();

        return $result;
    }

    //select concat(member_fname,' ',member_mname,' ',member_lname), member_date_of_birth, member_relationship,
    // member_occupation from onboarding_family_details  where users_id = 6 and is_active = 1;

    public function getUsersFamilyDetailsForPDF($users_id) {

        $this->db->select('concat(member_fname," ",member_mname," ",member_lname) as member_name, DATE_FORMAT(member_date_of_birth, "%d-%b-%Y") as member_date_of_birth, member_relationship, member_occupation');
        $this->db->from('onboarding_family_details');
        $this->db->where('users_id', $users_id);
        $this->db->where('is_active', 1);
        // $this->db->order_by("pro_qualification_passing_year", "desc");

        $result = $this->db->get()->result_array();

        return $result;
    }

    //select experience_organization, experience_period_from_month, experience_period_from_year, experience_period_to_month, experience_period_to_year, 
    //experience_designation, experience_job_responsibility, experience_reason_for_leaving  from onboarding_experience where users_id = 6 and is_active = 1 
    //order by experience_period_from_year desc, experience_period_to_year desc, experience_period_to_month ;

    public function getUsersExperienceDetailsForPDF($users_id) {

        $this->db->select('experience_organization, experience_period_from_month, experience_period_from_year, experience_period_to_month, experience_period_to_year,
							experience_designation, experience_job_responsibility, experience_gross_salary_month, experience_reason_for_leaving,
                                                        (select sum(total_experience) as total from onboarding_experience where users_id = '. $users_id .') as total_experience');
        $this->db->from('onboarding_experience');
        $this->db->where('users_id', $users_id);
        $this->db->where('is_active', 1);
        $this->db->order_by("experience_period_from_year desc, experience_period_to_year desc, experience_period_to_month");

        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getUserDetailsForPDFFinal($user_id) {
        
        $data1 = Array();
        $data1['reference'] = $this->getUsersDetailsReferencesForPDF($user_id);
        $data1['experience'] = $this->getUsersExperienceDetailsForPDF($user_id);
        $data1['family'] = $this->getUsersFamilyDetailsForPDF($user_id);
        $data1['proQualification'] = $this->getUsersDetailsProQualificationForPDF($user_id);        
        $data1['eduQualification'] = $this->getUsersDetailsEduQualificationForPDF($user_id);        
        $data1['userDetails'] = $this->getUsersDetailsForPDF($user_id)[0];        
        $data1['childrens'] = $this->get_children_info($data1['family']);
        
        return $data1;
    }
    
    private function get_children_info($data) {
        
        $child_data = array();
                        
        if(isset($data)){
            $i = 0;
            foreach ($data as $key => $value) {
                if($value['member_relationship'] == 'Children' || $value['member_relationship'] == 'Son' || $value['member_relationship'] == 'Daughter'){
                    $child_data[$i] = $value;
                    $i++;
                }
            }
        }
        
        return $child_data;
    }
    
    public function getUsersIdsByJoiningDate($joining_date){
		
		$this->db->select('users_id');
		$this->db->from('onboarding');
        $this->db->where('date_of_joining', $joining_date);
        $this->db->where('is_active', 1);
        $this->db->group_by('users_id');
        $this->db->order_by('created_on');
        $result = $this->db->get()->result_array();
        
        return $result;
    }    

    /**
     * This function used to save login information of user
     * @param array $loginInfo : This is users login information
     */
    public function lastLogin($loginInfo){

        $this->db->trans_start();
        $this->db->insert('tbl_last_login', $loginInfo);
        $this->db->trans_complete();
    }

    /**
     * This function is used to get last login info by user id
     * @param number $userId : This is user id
     * @return number $result : This is query result
     */
    public function lastLoginInfo($userId){

        $this->db->select('createdDtm');
		$this->db->from('tbl_last_login');
        $this->db->where('users_id', $userId);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();

        //$str = $this->db->last_query();
                
        return $result;
    }
}