<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Documents_model Class extends CI_Model
 */
class Onboarding_model extends CI_Model {
    
    function __construct() {
        
        parent::__construct();
        $this->load->database();
    }

    public function insert_or_update_onboarding_step1($users_id, $data) {
                
        $data["updated_by"] = $users_id;
        
        $this->db->where("users_id", $users_id);
        $this->db->update("onboarding", $data);
                
        $update_status = $this->db->affected_rows();
        
        if($update_status <= 0){
            
            $data["users_id"] = $users_id;
            $data["created_by"] = $users_id;            
            
            $this->db->insert("onboarding", $data);            
        }

        //print_r($this->db->last_query());
        
        return true;
    }

    /**
     * This function is used to get onboarding step 1 data 
     */
    public function get_onboarding_step1_data($users_id) {

        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $result = $this->db->get('onboarding')->result_array();

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            $result_revised_offer = $this->db->get('revised_offer_details')->result_array();

            if(count($result_revised_offer) > 0){
                $result[0]['date_of_joining'] = $result_revised_offer[0]['date_of_joining_by_hr'];                
            }else{
                $this->db->where('users_id', $users_id);
                $user_result = $this->db->get('users')->result_array();
                
                $result[0]['date_of_joining'] = $user_result[0]['date_of_joining_by_hr'];
            }

            return $result;
        }
        
        return FALSE;
    }


    public function insert_or_update_onboarding_step2($users_id, $data){
        $data_active_status = array();
        $data_active_status["updated_by"] = $users_id;
        // $data_active_status["is_active"] = FALSE;

        // $this->db->where("users_id", $users_id);       
        // $this->db->update("onboarding_general_documents", $data_active_status);
        
        foreach($data as $key => $value){

            $this->db->where("users_id", $users_id);
            $this->db->where("field_id", $value['field_id']);
            $this->db->update('onboarding_general_documents', $value);

            $update_status = $this->db->affected_rows();            
            if($update_status <= 0){
                $value["users_id"] = $users_id;
                $value["created_by"] = $users_id;

                $this->db->insert("onboarding_general_documents", $value); 
            }
        }
        
        return true;
    }

    /**
     * This function is used to get onboarding step 2 data 
     */
    public function get_onboarding_step2_data($users_id) {

        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            return $this->db->get('onboarding_general_documents')->result_array();
        }

        return FALSE;
    }
    
    public function insert_or_update_onboarding_step3($users_id, $data) {
        
        $data_active_status = array();
        $data["updated_by"] = $users_id;
        $data_active_status["updated_by"] = $users_id;
        $data_active_status["is_active"] = FALSE;
        
        $this->db->where("users_id", $users_id);       
        $this->db->update("onboarding_family_details", $data_active_status);
        
        foreach ($data as $key => $value) {
            $this->db->where("users_id", $users_id); 
            $this->db->where("field_id", $key);
            $value["users_id"] = $users_id;
            $value["updated_by"] = $users_id;
            $value["is_active"] = TRUE;
            
            $this->db->update("onboarding_family_details", $value);                        
            
            $update_status = $this->db->affected_rows();            
            if($update_status <= 0){
                $value["users_id"] = $users_id;
                $value["created_by"] = $users_id;     
                $value["field_id"] = $key;     

                $this->db->insert("onboarding_family_details", $value); 
            }
            
            //print_r($this->db->last_query());
        }
                      
        return true;
    }
    
    public function get_onboarding_step3_data($users_id) {
        
        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            $this->db->order_by('field_id', 'asc');
            return $this->db->get('onboarding_family_details')->result_array();
        }

        return FALSE;
    }
    
    public function insert_or_update_onboarding_step4($users_id, $data) {
        
        $data_active_status = array();
        $data["updated_by"] = $users_id;
        $data_active_status["updated_by"] = $users_id;
        $data_active_status["is_active"] = FALSE;
        
        $this->db->where("users_id", $users_id);       
        $this->db->update("onboarding_educational_qualification", $data_active_status);
        
        foreach ($data as $key => $value) {
            
            $this->db->where("users_id", $users_id);            
            $this->db->where("field_id", $key);
            $value["users_id"] = $users_id;
            $value["updated_by"] = $users_id;
            $value["is_active"] = TRUE;
            
            $this->db->update("onboarding_educational_qualification", $value);                        
            
            $update_status = $this->db->affected_rows();            
            if($update_status <= 0){
                $value["users_id"] = $users_id;
                $value["created_by"] = $users_id;     
                $value["field_id"] = $key;     

                $this->db->insert("onboarding_educational_qualification", $value); 
            }
        }
                        
        return true;
    }
    
    public function get_onboarding_step4_data($users_id) {
        
        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            $this->db->order_by('field_id', 'asc');
            return $this->db->get('onboarding_educational_qualification')->result_array();
        }

        return FALSE;
    }

    public function insert_or_update_onboarding_step5($users_id, $data){

        $data_active_status = array();
        $data["updated_by"] = $users_id;
        $data_active_status["updated_by"] = $users_id;
        $data_active_status["is_active"] = FALSE;
        
        $this->db->where("users_id", $users_id);       
        $this->db->update("onboarding_professional_qualification", $data_active_status);

        foreach ($data as $key => $value) {
            
            $this->db->where("users_id", $users_id);            
            $this->db->where("field_id", $key);
            $value["users_id"] = $users_id;
            $value["updated_by"] = $users_id;
            $value["is_active"] = TRUE;
            
            $this->db->update("onboarding_professional_qualification", $value);                        
            
            $update_status = $this->db->affected_rows();            
            if($update_status <= 0){
                $value["users_id"] = $users_id;
                $value["created_by"] = $users_id;     
                $value["field_id"] = $key;     

                $this->db->insert("onboarding_professional_qualification", $value); 
            }
        }
                        
        return true;
    }  
    
    public function get_onboarding_step5_data($users_id) {
        
        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            $this->db->order_by('field_id', 'asc');
            return $this->db->get('onboarding_professional_qualification')->result_array();
        }

        return FALSE;
    }

    public function insert_or_update_onboarding_step6($users_id, $data) {     

        $data_active_status = array();
        $data["updated_by"] = $users_id;
        $data_active_status["updated_by"] = $users_id;
        $data_active_status["is_active"] = FALSE;
        
        $this->db->where("users_id", $users_id);       
        $this->db->update("onboarding_experience", $data_active_status);

        foreach ($data as $key => $value) {
            
            $this->db->where("users_id", $users_id);            
            $this->db->where("field_id", $key);
            $value["users_id"] = $users_id;
            $value["updated_by"] = $users_id;
            $value["is_active"] = TRUE;
            
            $this->db->update("onboarding_experience", $value);                        
            
            $update_status = $this->db->affected_rows();            
            if($update_status <= 0){
                $value["users_id"] = $users_id;
                $value["created_by"] = $users_id;     
                $value["field_id"] = $key;     

                $this->db->insert("onboarding_experience", $value); 
                //print_r($this->db->last_query());
            }
        }
                        
        return true;
    }

    public function get_onboarding_step6_data($users_id) {
        
        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            $this->db->order_by('field_id', 'asc');
            return $this->db->get('onboarding_experience')->result_array();
        }

        return FALSE;
    }

    public function insert_or_update_onboarding_step6_ref($users_id, $data) {     

        $data_active_status = array();
        $data["updated_by"] = $users_id;
        $data_active_status["updated_by"] = $users_id;
        $data_active_status["is_active"] = FALSE;
        
        $this->db->where("users_id", $users_id);       
        $this->db->update("onboarding_references", $data_active_status);
        
        foreach ($data as $key => $value) {
            
            $this->db->where("users_id", $users_id);            
            $this->db->where("field_id", $key);
            $value["users_id"] = $users_id;
            $value["updated_by"] = $users_id;
            $value["is_active"] = TRUE;
            
            $this->db->update("onboarding_references", $value);                        
            
            $update_status = $this->db->affected_rows();            
            if($update_status <= 0){
                $value["users_id"] = $users_id;
                $value["created_by"] = $users_id;     
                $value["field_id"] = $key;     

                $this->db->insert("onboarding_references", $value);                
            }
        }
                        
        return true;
    }

    public function get_onboarding_step6_ref_data($users_id) {

        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            $this->db->order_by('field_id', 'asc');
            return $this->db->get('onboarding_references')->result_array();
        }

        return FALSE;
    }

    public function update_current_step_of_user($users_id, $data) {
        
        if (!empty($users_id)) {

            $this->db->where('users_id', $users_id);
            $this->db->where('is_active', TRUE);
            
            $data['stage_status'] = 'Onboarding';
            $this->db->update("users", $data);

            $update_status = $this->db->affected_rows();

            if($update_status > 0){
                return TRUE;
            }else{
                $this->db->where('users_id', $users_id);
                $this->db->where('is_active', TRUE);
                $this->db->where('current_step', $data['current_step']);
                $row_count = $this->db->get("users")->num_rows();
                //print_r($this->db->last_query());
                if($row_count == 1){
                    return TRUE;
                }
            }
            
            $db_error = $this->db->error();
            if (!empty($db_error)) {

                if($db_error['code'] != 0){
                    $this->session->set_flashdata('error', $db_error['message']);
                    return FALSE;
                }
            }
        }

        return FALSE;
    }
}