<!-- page content -->
<!-- Content Wrapper. Contains page content -->
<div id="content-outer-div" class="content-wrapper onboarding-page">
    <!-- Main content -->
    <section class="content">
        <?php
        $email_id = strtolower($this->session->get_userdata()['user_details'][0]->email);
        $emp_experience = $this->session->get_userdata()['user_details'][0]->emp_experience;        
        $current_step = $this->session->get_userdata()['current_step'];
        
        if ($this->session->flashdata("message")) { ?>
            <div class="alert <?php echo ($this->session->flashdata("message") == "Error") ? "alert-danger" : "alert-success" ?> alert-dismissible" role="alert">      
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata("message") ?>
            </div>
        <?php } ?>
        
        <?php
        $latest_year = date('Y');
        $earliest_year = $latest_year - 59; 
        ?>

        <input type="hidden" id="current-step" name="current_step" value="<?php echo $current_step; ?>" />

        <!-- Default box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Onboarding</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="stepwizard">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                    <p>Personal Info</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                    <p>General Documents</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                    <p>Family Info</p>
                                </div>                                
                                <div class="stepwizard-step">
                                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                                    <p>Educational Qualification</p>
                                </div>                                
                                <div class="stepwizard-step">
                                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                                    <p>Professional Qualification</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                                    <p><?php if(isset($emp_experience) && $emp_experience == 'Yes'){ echo 'Experience & '; } ?>References</p>
                                </div>
                            </div>
                        </div>

                        <div class="row setup-content" id="step-1">
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'onboarding/save_onboarding_data_step1' ?>">
                                <div class="col-md-12">
                                    <h3>Personal Info</h3>

                                    <div class="row">
                                        <div class="col-md-12">Employee Name: </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">First Name <em>*</em></label>
                                                <input  maxlength="100" type="text" name="employee_fname" value="<?php echo isset($result['employee_fname']) ? $result['employee_fname'] : ''; ?>" required="required" class="form-control" placeholder="First Name"  />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Middle Name</label>
                                                <input maxlength="100" type="text" name="employee_mname" value="<?php echo isset($result['employee_mname']) ? $result['employee_mname'] : ''; ?>" class="form-control" placeholder="Middle Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Last Name <em>*</em></label>
                                                <input maxlength="100" type="text" name="employee_lname" value="<?php echo isset($result['employee_lname']) ? $result['employee_lname'] : ''; ?>" required="required" class="form-control" placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">Father's/ Husband's Name:</div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">First Name <em>*</em></label>
                                                <input  maxlength="100" type="text" name="father_fname" value="<?php echo isset($result['father_fname']) ? $result['father_fname'] : ''; ?>" required="required" class="form-control" placeholder="First Name"  />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Middle Name</label>
                                                <input maxlength="100" type="text" name="father_mname" value="<?php echo isset($result['father_mname']) ? $result['father_mname'] : ''; ?>" class="form-control" placeholder="Middle Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Last Name <em>*</em></label>
                                                <input maxlength="100" type="text" name="father_lname" value="<?php echo isset($result['father_lname']) ? $result['father_lname'] : ''; ?>" required="required" class="form-control" placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $blood_groups = array('A+', 'O+', 'B+', 'AB+', 'A-', 'O-', 'B-', 'AB-'); ?>
                                                <label class="control-label">Blood Group <em>*</em></label>
                                                <select name="blood_group" class="form-control" required="required">
                                                    <option value="" disabled="disabled" <?php echo empty($result['blood_group']) ? "selected" : ""; ?>>Please Blood Group</option>
                                                    <?php foreach ($blood_groups as $key => $value) { ?>
                                                        <option value="<?php echo $value; ?>" <?php echo ($value == $result['blood_group']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>                            
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $gender = array('Male', 'Female', 'Transgender'); ?>
                                                <label class="control-label">Gender <em>*</em></label>
                                                <select name="gender" class="form-control" required="required">
                                                    <option value="" disabled="disabled" <?php echo empty($result['gender']) ? "selected" : ""; ?>>Please Select Gender</option>
                                                    <?php foreach ($gender as $key => $value) { ?>
                                                        <option value="<?php echo $value; ?>" <?php echo ($value == $result['gender']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $marital_status = array('Divorced', 'Married', 'Separated', 'Unmarried', 'Widowed'); ?>
                                                <label class="control-label">Marital Status <em>*</em></label>
                                                <select name="marital_status" class="form-control" required="required">
                                                    <option value="" disabled="disabled" <?php echo empty($result['marital_status']) ? "selected" : ""; ?>>Please Select Status</option>
                                                    <?php foreach ($marital_status as $key => $value) { ?>
                                                        <option value="<?php echo $value; ?>" <?php echo ($value == $result['marital_status']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>

                                    <div id="spouse-information" class="row">
                                        <div class="col-md-12">Spouse Name:</div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">First Name <em>*</em></label>
                                                <input  maxlength="100" type="text" name="spouse_fname" value="<?php echo isset($result['spouse_fname']) ? $result['spouse_fname'] : ''; ?>" class="form-control" placeholder="First Name"  />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Middle Name</label>
                                                <input maxlength="100" type="text" name="spouse_mname" value="<?php echo isset($result['spouse_mname']) ? $result['spouse_mname'] : ''; ?>" class="form-control" placeholder="Middle Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Last Name <em>*</em></label>
                                                <input maxlength="100" type="text" name="spouse_lname" value="<?php echo isset($result['spouse_lname']) ? $result['spouse_lname'] : ''; ?>" class="form-control" placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
<!--                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Number of Children</label>
                                                <input maxlength="100" type="text" name="children_count" value="<?php //echo isset($result['children_count']) ? $result['children_count'] : '0'; ?>" pattern="[0-9]{1,}" class="form-control" placeholder="Children Count" />
                                            </div>
                                        </div>                                -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Contact #1 <em>*</em></label>
                                                <input type="tel" required="required" name="contact_no1" value="<?php echo isset($result['contact_no1']) ? $result['contact_no1'] : ''; ?>" pattern="[0-9+ ]{10,}" class="form-control" placeholder="Contact Number #1" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Contact #2</label>
                                                <input type="tel" name="contact_no2" value="<?php echo isset($result['contact_no2']) ? $result['contact_no2'] : ''; ?>" pattern="[0-9+ ]{10,}" class="form-control" placeholder="Contact Number #2" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Email ID</label>
                                                <input maxlength="100" type="email" name="email_id" value="<?php echo $email_id; ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$" class="form-control" placeholder="Email ID" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Date of Birth <em>*</em></label>
                                                <input type="date" required="required" id="dob-of-emp" name="date_of_birth" value="<?php echo isset($result['date_of_birth']) ? $result['date_of_birth'] : ''; ?>" class="date-picker-for-birth-date form-control" placeholder="DD/MM/YYYY" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Age</label>
                                                <input maxlength="100" type="text" name="age" id="calculate-age-from-dob" value="<?php echo isset($result['age']) ? $result['age'] : ''; ?>" class="form-control" placeholder="Age" readonly />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $nationality = array('Afghan', 'Albanian', 'Algerian', 'American', 'Andorran', 'Angolan', 'Antiguans', 'Argentinean', 'Armenian', 'Australian', 'Austrian', 'Azerbaijani', 'Bahamian', 'Bahraini', 'Bangladeshi', 'Barbadian', 'Barbudans', 'Batswana', 'Belarusian', 'Belgian', 'Belizean', 'Beninese', 'Bhutanese', 'Bolivian', 'Bosnian', 'Brazilian', 'British', 'Bruneian', 'Bulgarian', 'Burkinabe', 'Burmese', 'Burundian', 'Cambodian', 'Cameroonian', 'Canadian', 'Cape Verdean', 'Central African', 'Chadian', 'Chilean', 'Chinese', 'Colombian', 'Comoran', 'Congolese', 'Costa Rican', 'Croatian', 'Cuban', 'Cypriot', 'Czech', 'Danish', 'Djibouti', 'Dominican', 'Dutch', 'East Timorese', 'Ecuadorean', 'Egyptian', 'Emirian', 'Equatorial Guinean', 'Eritrean', 'Estonian', 'Ethiopian', 'Fijian', 'Filipino', 'Finnish', 'French', 'Gabonese', 'Gambian', 'Georgian', 'German', 'Ghanaian', 'Greek', 'Grenadian', 'Guatemalan', 'Guinea-Bissauan', 'Guinean', 'Guyanese', 'Haitian', 'Herzegovinian', 'Honduran', 'Hungarian', 'Icelander', 'Indian', 'Indonesian', 'Iranian', 'Iraqi', 'Irish', 'Israeli', 'Italian', 'Ivorian', 'Jamaican', 'Japanese', 'Jordanian', 'Kazakhstani', 'Kenyan', 'Kittian And Nevisian', 'Kuwaiti', 'Kyrgyz', 'Laotian', 'Latvian', 'Lebanese', 'Liberian', 'Libyan', 'Liechtensteiner', 'Lithuanian', 'Luxembourger', 'Macedonian', 'Malagasy', 'Malawian', 'Malaysian', 'Maldivan', 'Malian', 'Maltese', 'Marshallese', 'Mauritanian', 'Mauritian', 'Mexican', 'Micronesian', 'Moldovan', 'Monacan', 'Mongolian', 'Moroccan', 'Mosotho', 'Motswana', 'Mozambican', 'Namibian', 'Nauruan', 'Nepalese', 'New Zealander', 'Ni-Vanuatu', 'Nicaraguan', 'Nigerien', 'North Korean', 'Northern Irish', 'Norwegian', 'Omani', 'Pakistani', 'Palauan', 'Panamanian', 'Papua New Guinean', 'Paraguayan', 'Peruvian', 'Polish', 'Portuguese', 'Qatari', 'Romanian', 'Russian', 'Rwandan', 'Saint Lucian', 'Salvadoran', 'Samoan', 'San Marinese', 'Sao Tomean', 'Saudi', 'Scottish', 'Senegalese', 'Serbian', 'Seychellois', 'Sierra Leonean', 'Singaporean', 'Slovakian', 'Slovenian', 'Solomon Islander', 'Somali', 'South African', 'South Korean', 'Spanish', 'Sri Lankan', 'Sudanese', 'Surinamer', 'Swazi', 'Swedish', 'Swiss', 'Syrian', 'Taiwanese', 'Tajik', 'Tanzanian', 'Thai', 'Togolese', 'Tongan', 'Trinidadian Or Tobagonian', 'Tunisian', 'Turkish', 'Tuvaluan', 'Ugandan', 'Ukrainian', 'Uruguayan', 'Uzbekistani', 'Venezuelan', 'Vietnamese', 'Welsh', 'Yemenite', 'Zambian', 'Zimbabwean'); ?>
                                                <label class="control-label">Nationality <em>*</em></label>
                                                <select name="nationality" class="form-control" required="required">
                                                    <option value="" disabled="disabled" <?php echo empty($result['nationality']) ? "selected" : ""; ?>>Select Nationality</option>
                                                    <?php foreach ($nationality as $key => $value) { ?>
                                                        <option value="<?php echo $value; ?>" <?php echo ($value == $result['nationality']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Hobbies</label>
                                                <input maxlength="100" type="text" name="hobbies" value="<?php echo isset($result['hobbies']) ? $result['hobbies'] : ''; ?>" class="form-control" placeholder="Your Hobbies" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Date of Joining <em>*</em></label>
                                                <?php $user_entered_doj = isset($result['date_of_joining']) ? $result['date_of_joining'] : '';
                                                      $disabled_doj = !empty($user_entered_doj) ? 'readonly' : ''; ?>
                                                <input maxlength="100" type="date" required="required" id="date-of-joining" name="date_of_joining" value="<?php echo $user_entered_doj; ?>" pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" class="form-control" placeholder="DD/MM/YYYY" <?php echo $disabled_doj; ?> />
                                            </div>
                                        </div>                                        
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Contact Address <em>*</em></label>
                                                <textarea required="required" name="contact_address" class="form-control" placeholder="Your contact address" ><?php echo isset($result['contact_address']) ? $result['contact_address'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">City <em>*</em></label>
                                                <input maxlength="30" type="text" name="contact_city" value="<?php echo isset($result['contact_city']) ? $result['contact_city'] : ''; ?>" class="form-control" placeholder="City" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">State <em>*</em></label>
                                                <input maxlength="30" type="text" name="contact_state" value="<?php echo isset($result['contact_state']) ? $result['contact_state'] : ''; ?>" class="form-control" placeholder="State" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Pin Code <em>*</em></label>
                                                <input maxlength="8" type="text" name="contact_pin_code" value="<?php echo isset($result['contact_pin_code']) ? $result['contact_pin_code'] : ''; ?>" pattern="\d{6,}" class="form-control" placeholder="Pin Code" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Permanent Address <em>*</em></label>
                                                <textarea required="required" name="permanent_address" class="form-control" placeholder="Your permanent address"><?php echo isset($result['permanent_address']) ? $result['permanent_address'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">City <em>*</em></label>
                                                <input maxlength="30" type="text" name="permanent_city" value="<?php echo isset($result['permanent_city']) ? $result['permanent_city'] : ''; ?>" class="form-control" placeholder="City" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">State <em>*</em></label>
                                                <input maxlength="30" type="text" name="permanent_state" value="<?php echo isset($result['permanent_state']) ? $result['permanent_state'] : ''; ?>" class="form-control" placeholder="State" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Pin Code <em>*</em></label>
                                                <input maxlength="8" type="text" name="permanent_pin_code" value="<?php echo isset($result['permanent_pin_code']) ? $result['permanent_pin_code'] : ''; ?>" pattern="\d{6,}" class="form-control" placeholder="Pin Code" required="required" />
                                            </div>
                                        </div>
                                    </div>

                                    <?php $pan_aadhaar_card = array('Having', 'Applied'); ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label nri-employee">Pan Card <em>*</em></label>                                                
                                                <select name="pan_card_id" class="form-control" <?php echo ($result['nationality'] == 'Indian') ? 'required' : ''; ?>>
                                                    <option value="" disabled="disabled" <?php echo empty($result['pan_card_id']) ? "selected" : ""; ?>>Select Preferred Option</option>
                                                    <?php foreach ($pan_aadhaar_card as $key => $value) { ?>
                                                        <option value="<?php echo $value; ?>" <?php echo ($value == $result['pan_card_id']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>                                              
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label nri-employee-panid">Pan Number <em>*</em></label>
                                                <input maxlength="10" type="text" name="pan_number" value="<?php echo isset($result['pan_number']) ? $result['pan_number'] : ''; ?>" pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" class="form-control" placeholder="Pan Number" <?php echo ($result['pan_card'] == 'Having') ? 'required' : ''; ?> />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label nri-employee">Aadhaar Card <em>*</em></label>                                                
                                                <select name="aadhaar_card" class="form-control" <?php echo ($result['nationality'] == 'Indian') ? 'required' : ''; ?>>
                                                    <option value="" disabled="disabled" <?php echo empty($result['aadhaar_card']) ? "selected" : ""; ?>>Select Preferred Option</option>
                                                    <?php foreach ($pan_aadhaar_card as $key => $value) { ?>
                                                    <option value="<?php echo $value; ?>" <?php echo ($value == $result['aadhaar_card']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>                                              
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label nri-employee-aadhaarid">Aadhaar Number <em>*</em></label>
                                                <input maxlength="12" type="text" name="aadhaar_number" value="<?php echo isset($result['aadhaar_number']) ? $result['aadhaar_number'] : ''; ?>" pattern="[0-9]{12}" class="form-control" placeholder="Aadhaar Number" <?php echo ($result['aadhaar_card'] == 'Having') ? 'required' : ''; ?> />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $pf_number_exist = array('Yes', 'No'); ?>
                                                <label class="control-label">PF Number <em>*</em></label>                                                
                                                <select name="pf_number_exist" class="form-control" required="required">
                                                    <option value="" disabled="disabled" <?php echo empty($result['pf_number_exist']) ? "selected" : ""; ?>>Select Preferred Option</option>
                                                    <?php foreach ($pf_number_exist as $key => $value) { ?>
                                                    <option value="<?php echo $value; ?>" <?php echo ($value == $result['pf_number_exist']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>                                              
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label employee-pf-number">PF Number <em>*</em></label>
                                                <input maxlength="30" type="text" name="pf_number" value="<?php echo isset($result['pf_number']) ? $result['pf_number'] : ''; ?>" class="form-control" placeholder="PF Number" <?php ($result['pf_number_exist'] == 'Yes') ? "required" : ''; ?> />
                                            </div>
                                        </div>                       
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label employee-pf-number">UAN Number <em>*</em></label>
                                                <input maxlength="12" type="text" name="uan_number" value="<?php echo isset($result['uan_number']) ? $result['uan_number'] : ''; ?>" pattern="[0-9]{12}" class="form-control" placeholder="UAN Number" <?php ($result['pf_number_exist'] == 'Yes') ? "required" : ''; ?> />
                                            </div>
                                        </div>                       
                                    </div>

                                    <div id="passport-information" class="row nri-employee-passport">
                                        <div class="col-md-12"><b>Passport Information: </b></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Passport Number <em>*</em></label>
                                                <input type="text" name="passport_number" value="<?php echo isset($result['passport_number']) ? $result['passport_number'] : ''; ?>" class="form-control" placeholder="Passport Number"  />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Issue Date <em>*</em></label>
                                                <input type="text" name="passport_issue_date" value="<?php echo isset($result['passport_issue_date']) ? $result['passport_issue_date'] : ''; ?>" pattern="[0-9+ ]{2}/[0-9+ ]{4}" class="form-control" placeholder="MM/YYYY"  />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Expiry Date <em>*</em></label>
                                                <input type="text" name="passport_expiry_date" value="<?php echo isset($result['passport_expiry_date']) ? $result['passport_expiry_date'] : ''; ?>" pattern="[0-9+ ]{2}/[0-9+ ]{4}" class="form-control" placeholder="MM/YYYY"  />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12"><b>ICE (In Case of Emergency): </b></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">First Name <em>*</em></label>
                                                <input  maxlength="100" type="text" name="ice_fname" value="<?php echo isset($result['ice_fname']) ? $result['ice_fname'] : ''; ?>" required="required" class="form-control" placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Middle Name</label>
                                                <input maxlength="100" type="text" name="ice_mname" value="<?php echo isset($result['ice_mname']) ? $result['ice_mname'] : ''; ?>" class="form-control" placeholder="Middle Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Last Name <em>*</em></label>
                                                <input maxlength="100" type="text" name="ice_lname" value="<?php echo isset($result['ice_lname']) ? $result['ice_lname'] : ''; ?>" required="required" class="form-control" placeholder="Last Name" />
                                            </div>
                                        </div>                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">ICE Contact Number(Should not be your own personal number) <em>*</em></label>
                                                <input type="tel" name="ice_contact_no" value="<?php echo isset($result['ice_contact_no']) ? $result['ice_contact_no'] : ''; ?>" required="required" pattern="[0-9+ ]{10,}" class="form-control" placeholder="ICE Contact Number" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <?php $ice_relationship = array('Brother', 'Father', 'Friend', 'Mother', 'Relative', 'Sister', 'Spouse'); ?>
                                            <div class="form-group">
                                                <label class="control-label">ICE Contact Relationship <em>*</em></label>
                                                <select name="ice_relationship" class="form-control" required="required">
                                                    <option value="" disabled="disabled" <?php echo empty($result['ice_relationship']) ? "selected" : ""; ?>>Select Relationship</option>
                                                    <?php foreach ($ice_relationship as $key => $value) { ?>
                                                        <option value="<?php echo $value; ?>" <?php echo ($value == $result['ice_relationship']) ? "selected" : ""; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary nextBtn btn-sm pull-right <?php echo ($current_step == 1) ? "cursor-not-allowed" : ""; ?>" type="button" <?php echo ($current_step == 1) ? "disabled" : ""; ?>>Next</button>    
                                    <input type="submit" name="submit" class="btn btn-success btn-sm pull-right" value="Save Changes" />
                                </div>
                            </form>
                        </div>

                        <div class="row setup-content" id="step-2">
                            <div class="col-md-12">
                                <h3>General Documents</h3>

                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'onboarding/save_onboarding_data_step2' ?>">
                                    <div class="row">
                                        <div class="col-md-12 general-documents">
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Salary Slip #1</label>
                                                            <input type="text" name="salary_slip1_org" value="<?php echo isset($step2['salary_slip1']) ? $step2['salary_slip1'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="salary_slip1" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Salary Slip #2</label>
                                                            <input type="text" name="salary_slip2_org" value="<?php echo isset($step2['salary_slip2']) ? $step2['salary_slip2'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="salary_slip2" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Salary Slip #3</label>
                                                            <input type="text" name="salary_slip3_org" value="<?php echo isset($value['salary_slip3']) ? $value['salary_slip3'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="salary_slip3" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label nri-employee-aadhaarid">Aadhaar Card Front <em>*</em></label>
                                                            <input type="text" name="aadhaar_card_front_org" value="<?php echo isset($step2['aadhaar_card_front']) ? $step2['aadhaar_card_front'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="aadhaar_card_front" class="form-control" <?php echo empty($step2['aadhaar_card_front']) ? 'required' : ''; ?> />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Aadhaar Card Back</label>
                                                            <input type="text" name="aadhaar_card_back_org" value="<?php echo isset($step2['aadhaar_card_back']) ? $step2['aadhaar_card_back'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="aadhaar_card_back" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label nri-employee-panid">Pan Card <em>*</em></label>
                                                            <input type="text" name="pan_card_org" value="<?php echo isset($step2['pan_card']) ? $step2['pan_card'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="pan_card" class="form-control" <?php echo empty($step2['pan_card']) ? 'required' : ''; ?> />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Date of Birth Certificate</label>
                                                            <input type="text" name="dob_certificate_org" value="<?php echo isset($step2['dob_certificate']) ? $step2['dob_certificate'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="dob_certificate" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Latest Salary Revision Letter</label>
                                                            <input type="text" name="latest_salary_revision_letter_org" value="<?php echo isset($step2['latest_salary_revision_letter']) ? $step2['latest_salary_revision_letter'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="latest_salary_revision_letter" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Bank Statement (Salary Account)</label>
                                                            <input type="text" name="bank_statement_org" value="<?php echo isset($step2['bank_statement']) ? $step2['bank_statement'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="bank_statement" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group upload-files-custom">
                                                            <label class="control-label">Form 16 (if applicable)</label>
                                                            <input type="text" name="form_16_org" value="<?php echo isset($step2['form_16']) ? $step2['form_16'] : ''; ?>" class="form-control" readonly />
                                                            <input type="file" name="form_16" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                               

                                    <button class="btn btn-primary prevBtn btn-sm pull-left" type="button" >Prev</button>                                            
                                    <button class="btn btn-primary nextBtn btn-sm pull-right <?php echo ($current_step == 2) ? "cursor-not-allowed" : ""; ?>" type="button" <?php echo ($current_step == 2) ? "disabled" : ""; ?> >Next</button>
                                    <input type="submit" name="submit" class="btn btn-success btn-sm pull-right" value="Save Changes" />
                                </form>
                            </div>
                        </div>

                        <div class="row setup-content" id="step-3">
                            <div class="col-md-12">
                                <h3>Family Info</h3>
                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'onboarding/save_onboarding_data_step3' ?>">
                                    <div class="row">
                                        <div class="col-md-12 family-member-details">
                                            <div id="family-boxes-group">                                                
                                                <?php foreach ($step3 as $key => $value) { ?>
                                                    <div class="box-div-wrapper">
                                                        <div class="glyphicon glyphicon-remove custom-remove-button"></div>
                                                        <div class="row">
                                                            <div class="col-md-12"><label class="control-label text-bold">Family Member: </label></div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">First Name <em>*</em></label>
                                                                    <input  maxlength="100" type="text" name="member_fname[]" value="<?php echo isset($value['member_fname']) ? $value['member_fname'] : ''; ?>" required="required" class="form-control" placeholder="First Name" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Middle Name</label>
                                                                    <input maxlength="100" type="text" name="member_mname[]" value="<?php echo isset($value['member_mname']) ? $value['member_mname'] : ''; ?>" class="form-control" placeholder="Middle Name" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Last Name <em>*</em></label>
                                                                    <input maxlength="100" type="text" name="member_lname[]" value="<?php echo isset($value['member_lname']) ? $value['member_lname'] : ''; ?>" required="required" class="form-control" placeholder="Last Name" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <?php $member_relationship = array('Father', 'Mother', 'Spouse', 'Son', 'Daughter'); ?>

                                                                    <label class="control-label">Relationship <em>*</em></label>
                                                                    <select name="member_relationship[]" class="form-control member-relationship" required="required">
                                                                        <option value="" disabled="disabled" <?php echo empty($value['member_relationship']) ? "selected" : ""; ?>>Select Relationship</option>
                                                                        <?php foreach ($member_relationship as $key1 => $value1) { ?>
                                                                                <option value="<?php echo $value1; ?>" <?php echo ($value1 == $value['member_relationship']) ? "selected" : ""; ?>><?php echo $value1; ?></option>
                                                                        <?php }

                                                                        if($value['member_relationship'] == 'Children'){ ?>
                                                                            <option value="Children" selected>Children</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Date of Birth <em>*</em></label>
                                                                    <input type="date" name="member_date_of_birth[]" value="<?php echo isset($value['member_date_of_birth']) ? $value['member_date_of_birth'] : ''; ?>" required="required" pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" class="date-picker-for-birth-date form-control" placeholder="DD/MM/YYYY" />                                        
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <?php
                                                                $required_show = (($value['member_relationship'] != 'Children') && ($value['member_relationship'] != 'Son') && ($value['member_relationship'] != 'Daughter')) ? 'show-required' : '';
                                                                $input_field_required = (($value['member_relationship'] != 'Children') && ($value['member_relationship'] != 'Son') && ($value['member_relationship'] != 'Daughter')) ? 'required' : ''; ?>
                                                                <div class="form-group">
                                                                    <label class="control-label occupation-required <?php echo $required_show; ?>">Occupation <em>*</em></label>
                                                                    <input maxlength="100" type="text" name="member_occupation[]" value="<?php echo isset($value['member_occupation']) ? $value['member_occupation'] : ''; ?>" class="form-control family-member-occupation" placeholder="Occupation" <?php echo $input_field_required; ?> />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group upload-files-custom">
                                                                    <label class="control-label">Aadhaar Card Front</label>
                                                                    <input type="text" name="member_aadhaar_card_front_org" value="<?php echo isset($value['member_aadhaar_card_front_org']) ? $value['member_aadhaar_card_front_org'] : ''; ?>" class="form-control" readonly />
                                                                    <input type="file" name="member_aadhaar_card_front[]" value="<?php echo isset($value['member_aadhaar_card_front']) ? $value['member_aadhaar_card_front'] : ''; ?>" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group upload-files-custom">
                                                                    <label class="control-label">Aadhaar Card Back</label>
                                                                    <input type="text" name="member_aadhaar_card_back_org" value="<?php echo isset($value['member_aadhaar_card_back_org']) ? $value['member_aadhaar_card_back_org'] : ''; ?>" class="form-control" readonly />
                                                                    <input type="file" name="member_aadhaar_card_back[]" value="<?php echo isset($value['member_aadhaar_card_back']) ? $value['member_aadhaar_card_back'] : ''; ?>" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="button" value="Add Family Member" id="addFamilyMemberButton" class="btn btn-sm btn-secondary" />
                                                    <!-- <input type="button" value="Remove Family Member" id="removeFamilyMemberButton" class="btn btn-sm btn-danger" /> -->
                                                </div>
                                            </div>

                                            <button class="btn btn-primary prevBtn btn-sm pull-left" type="button" >Prev</button>  
                                            <button class="btn btn-primary nextBtn btn-sm pull-right <?php echo ($current_step == 3) ? "cursor-not-allowed" : ""; ?>" type="button" <?php echo ($current_step == 3) ? "disabled" : ""; ?> >Next</button>                                          
                                            <!-- <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button> -->
                                            <input type="submit" name="submit" class="btn btn-success btn-sm pull-right" value="Save Changes" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                        <div class="row setup-content" id="step-4">
                            <div class="col-md-12">
                                <h3>Educational Qualification</h3>

                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'onboarding/save_onboarding_data_step4' ?>">
                                    <div class="row">
                                        <div class="col-md-12 educational-documents">
                                            <div id="EduQualificationBoxesGroup">
                                                <?php
                                                $edu_qualification = array('SSC', 'HSC', 'Graduate', 'Post Graduate', 'Other');

                                                foreach ($step4 as $key => $value) {
                                                    ?>
                                                    <div class="box-div-wrapper">
                                                        <div class="glyphicon glyphicon-remove custom-remove-button"></div>
                                                        <div class="row">
                                                            <div class="col-md-12"><label class="control-label text-bold">Educational Qualification: </label></div>
                                                            <div class="col-md-4">                                                            
                                                                <div class="form-group">
                                                                    <label class="control-label">Qualification <em>*</em></label>
                                                                    <select name="edu_qualification[]" class="form-control" required="required">                                                    
                                                                        <option value="" disabled="disabled" <?php echo empty($step4['edu_qualification']) ? "selected" : ""; ?>>Select Qualification</option>
                                                                        <?php foreach ($edu_qualification as $key1 => $value1) { ?>
                                                                            <option value="<?php echo $value1; ?>" <?php echo ($value1 == $value['edu_qualification']) ? "selected" : ""; ?>><?php echo $value1; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 edu-other-qualification <?php echo (strtolower($value['edu_qualification']) == 'other') ? '' : 'hide-other-field-for-other-case'; ?>">
                                                                <div class="form-group">
                                                                    <label class="control-label">Qualification Name <em>*</em></label>
                                                                    <input type="text" name="edu_qualification_other[]" value="<?php echo isset($value['edu_qualification_other']) ? $value['edu_qualification_other'] : ''; ?>" class="form-control other-field-input" placeholder="Qualification" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">University</label>
                                                                    <input type="text" name="edu_qualification_university[]" value="<?php echo isset($value['edu_qualification_university']) ? $value['edu_qualification_university'] : ''; ?>" class="form-control" placeholder="University" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Passing Year</label>
                                                                            <select name="edu_qualification_passing_year[]" class="form-control" required="required">
                                                                                <option value="" disabled="disabled" <?php echo empty($value['edu_qualification_passing_year']) ? "selected" : ""; ?>>Year</option>
                                                                                <?php
                                                                                foreach ( range( $latest_year, $earliest_year ) as $year ) {
                                                                                    $selected = (isset($value['edu_qualification_passing_year']) && $value['edu_qualification_passing_year'] == $year) ? 'selected' : '';
                                                                                    echo '<option ' . $selected . ' value="' . $year . '" >' . $year . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Marks % or Grade</label>
                                                                            <input type="text" name="edu_qualification_percentage_marks[]" value="<?php echo isset($value['edu_qualification_percentage_marks']) ? $value['edu_qualification_percentage_marks'] : ''; ?>" class="form-control" placeholder="Marks % or Grade" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Specialization</label>
                                                                    <input type="text" name="edu_qualification_specialization[]" value="<?php echo isset($value['edu_qualification_specialization']) ? $value['edu_qualification_specialization'] : ''; ?>" class="form-control" placeholder="Specialization" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group upload-files-custom">
                                                                    <label class="control-label">Upload Marksheet/ Degree Certificate <em>*</em></label>
                                                                    <input type="text" name="edu_qualification_docs_org[]" value="<?php echo isset($value['edu_qualification_docs_org']) ? $value['edu_qualification_docs_org'] : ''; ?>" class="form-control" readonly />
                                                                    <input type="file" name="edu_qualification_docs[]" <?php echo!empty($value['edu_qualification_docs_org']) ? '' : 'required'; ?> class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <input type="button" value="Add Educational Qualification" id="addDocsButton" class="btn btn-sm btn-secondary" />
                                            <!-- <input type="button" value="Remove Qualification" id="removeDocsButton" class="btn btn-sm btn-danger" />                                 -->
                                        </div>
                                    </div>

                                    <button class="btn btn-primary prevBtn btn-sm pull-left" type="button" >Prev</button>
                                    <button class="btn btn-primary nextBtn btn-sm pull-right <?php echo ($current_step == 4) ? "cursor-not-allowed" : ""; ?>" type="button" <?php echo ($current_step == 4) ? "disabled" : ""; ?> >Next</button>
                                    <!-- <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button> -->
                                    <input type="submit" name="submit" class="btn btn-success btn-sm pull-right" value="Save Changes" />
                                </form>
                            </div>
                        </div>

                        <div class="row setup-content" id="step-5">
                            <div class="col-md-12">
                                <h3>Professional Qualification</h3>

                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'onboarding/save_onboarding_data_step5' ?>">
                                    <div class="row">
                                        <div class="col-md-12 professional-documents">
                                            <div id="ProfQualificationBoxesGroup">
                                                <?php foreach ($step5 as $key => $value) { ?>
                                                    <div class="box-div-wrapper">
                                                        <div class="glyphicon glyphicon-remove custom-remove-button"></div>
                                                        <div class="row">
                                                            <div class="col-md-12"><label class="control-label text-bold">Professional Qualification: </label></div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Qualification</label>
                                                                    <input type="text" name="pro_qualification[]" value="<?php echo isset($value['pro_qualification']) ? $value['pro_qualification'] : ''; ?>" class="form-control" placeholder="Qualification" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">University/Institute</label>
                                                                    <input type="text" name="pro_qualification_university[]" value="<?php echo isset($value['pro_qualification_university']) ? $value['pro_qualification_university'] : ''; ?>" class="form-control" placeholder="University" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">                                                                    
                                                                    <label class="control-label">Passing Year</label>
                                                                    <select name="pro_qualification_passing_year[]" class="form-control">
                                                                        <option value="" disabled="disabled" <?php echo empty($value['pro_qualification_passing_year']) ? "selected" : ""; ?>>Year</option>
                                                                        <?php
                                                                        foreach ( range( $latest_year, $earliest_year ) as $year ) {
                                                                            $selected = (isset($value['pro_qualification_passing_year']) && $value['pro_qualification_passing_year'] == $year) ? 'selected' : '';
                                                                            echo '<option ' . $selected . ' value="' . $year . '" >' . $year . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Marks % or Grade</label>
                                                                    <input type="text" name="pro_qualification_percentage_marks[]" value="<?php echo isset($value['pro_qualification_percentage_marks']) ? $value['pro_qualification_percentage_marks'] : ''; ?>" class="form-control" placeholder="Marks % or Grade" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Specialization</label>
                                                                    <input type="text" name="pro_qualification_specialization[]" value="<?php echo isset($value['pro_qualification_specialization']) ? $value['pro_qualification_specialization'] : ''; ?>" class="form-control" placeholder="Specialization" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group upload-files-custom">
                                                                    <label class="control-label">Upload Marksheet/ Degree Certificate</label>
                                                                    <input type="text" name="pro_qualification_docs_org[]" class="form-control" value="<?php echo isset($value['pro_qualification_docs_org']) ? $value['pro_qualification_docs_org'] : ''; ?>" readonly />
                                                                    <input type="file" name="pro_qualification_docs[]" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <input type="button" value="Add Professional Qualification" id="addProfDocsButton" class="btn btn-sm btn-secondary" />
                                            <!-- <input type="button" value="Remove Qualification" id="removeProfDocsButton" class="btn btn-sm btn-danger" /> -->
                                        </div>
                                    </div>                            
                                    <button class="btn btn-primary prevBtn btn-sm pull-left" type="button" >Prev</button>
                                    <!-- <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button> -->
                                    <button class="btn btn-primary nextBtn btn-sm pull-right <?php echo ($current_step == 5) ? "cursor-not-allowed" : ""; ?>" type="button" <?php echo ($current_step == 5) ? "disabled" : ""; ?> >Next</button>
                                    <input type="submit" name="submit" class="btn btn-success btn-sm pull-right" value="Save Changes" />
                                </form>
                            </div>
                        </div>

                        <div class="row setup-content" id="step-6">
                            <div class="col-md-12">
                                <?php if(isset($emp_experience) && $emp_experience == 'Yes'){ ?>
                                    <h3>Experience(Chronological order - at least last 3 years)</h3>
                                <?php } ?>

                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'onboarding/save_onboarding_data_step6' ?>">
                                    <?php if(isset($emp_experience) && $emp_experience == 'Yes'){ ?>
                                    <div class="row">                                        
                                        <div class="col-md-12 experience-details">
                                            <div id="ExperienceBoxesGroup">
                                                <?php foreach ($step6_experience as $key => $value) { ?>
                                                <div class="box-div-wrapper">
                                                    <div class="glyphicon glyphicon-remove custom-remove-button"></div>
                                                    <div class="row">
                                                        <div class="col-md-12"><label class="control-label text-bold">Experience: </label></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Organization <em>*</em></label>
                                                                <input type="text" name="experience_organization[]" value="<?php echo isset($value['experience_organization']) ? $value['experience_organization'] : ''; ?>" required="required" class="form-control" placeholder="Organization" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <?php
                                                                $MonthArray = array(
                                                                    "1" => "Jan", "2" => "Feb", "3" => "Mar", "4" => "Apr",
                                                                    "5" => "May", "6" => "Jun", "7" => "Jul", "8" => "Aug",
                                                                    "9" => "Sept", "10" => "Oct", "11" => "Nov", "12" => "Dec",
                                                                );
                                                                ?>   
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-md-12"><label class="control-label">Period From <em>*</em></label></div>
                                                                        <div class="col-md-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <select name="experience_period_from_year[]" class="form-control period-from-year" required="required">
                                                                                    <option value="" disabled="disabled" <?php echo empty($value['experience_period_from_year']) ? "selected" : ""; ?>>Year</option>
                                                                                    <?php
                                                                                    foreach ( range( $latest_year, $earliest_year ) as $year ) {
                                                                                        $selected = (isset($value['experience_period_from_year']) && $value['experience_period_from_year'] == $year) ? 'selected' : '';
                                                                                        echo '<option ' . $selected . ' value="' . $year . '" >' . $year . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <select name="experience_period_from_month[]" class="form-control period-from-month" required="required">
                                                                                    <option value="" disabled="disabled" <?php echo empty($value['experience_period_from_month']) ? "selected" : ""; ?>>Month</option>
                                                                                    <?php
                                                                                    foreach ($MonthArray as $monthNum => $month) {
                                                                                        $selected = (isset($value['experience_period_from_month']) && $value['experience_period_from_month'] == $monthNum) ? 'selected' : '';
                                                                                        echo '<option ' . $selected . ' value="' . $monthNum . '" >' . $month . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-md-12"><label class="control-label">Period To <em>*</em></label></div>
                                                                        <div class="col-md-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <select name="experience_period_to_year[]" class="form-control period-to-year" required="required">
                                                                                    <option value="" disabled="disabled" <?php echo empty($value['experience_period_to_year']) ? "selected" : ""; ?>>Year</option>
                                                                                    <?php
                                                                                    foreach ( range( $latest_year, $earliest_year ) as $year ) {
                                                                                        $selected = (isset($value['experience_period_to_year']) && $value['experience_period_to_year'] == $year) ? 'selected' : '';
                                                                                        echo '<option ' . $selected . ' value="' . $year . '" >' . $year . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-6">
                                                                            <div class="form-group">
                                                                                <select name="experience_period_to_month[]" class="form-control period-to-month" required="required">
                                                                                    <option value="" disabled="disabled" <?php echo empty($value['experience_period_to_month']) ? "selected" : ""; ?>>Month</option>
                                                                                    <?php
                                                                                    foreach ($MonthArray as $monthNum => $month) {
                                                                                        $selected = (isset($value['experience_period_to_month']) && $value['experience_period_to_month'] == $monthNum) ? 'selected' : '';
                                                                                        echo '<option ' . $selected . ' value="' . $monthNum . '">' . $month . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                                    
                                                                </div>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Total Experience(Months)</label>
                                                                <input type="text" name="total_experience[]" value="<?php echo isset($value['total_experience']) ? $value['total_experience'] : ''; ?>" class="form-control total-experience-single-org" placeholder="Total Experience" readonly />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Designation <em>*</em></label>
                                                                <input type="text" name="experience_designation[]" value="<?php echo isset($value['experience_designation']) ? $value['experience_designation'] : ''; ?>" required="required" class="form-control" placeholder="Designation" />
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Job Responsibility <em>*</em></label>
                                                                <input type="text" name="experience_job_responsibility[]" value="<?php echo isset($value['experience_job_responsibility']) ? $value['experience_job_responsibility'] : ''; ?>" required="required" class="form-control" placeholder="Job Responsibility" />
                                                            </div>
                                                        </div> -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Gross Salary/Month <em>*</em></label>
                                                                <input type="text" name="experience_gross_salary_month[]" value="<?php echo isset($value['experience_gross_salary_month']) ? $value['experience_gross_salary_month'] : ''; ?>" required="required" class="form-control" placeholder="Gross Salary/Month" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Reason For Leaving <em>*</em></label>
                                                                <input type="text" name="experience_reason_for_leaving[]" value="<?php echo isset($value['experience_reason_for_leaving']) ? $value['experience_reason_for_leaving'] : ''; ?>" required="required" class="form-control" placeholder="Reason For Leaving" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group upload-files-custom">
                                                                <label class="control-label">Relieving Letter</label>
                                                                <input type="text" name="experience_relieving_letter_org[]" value="<?php echo isset($value['experience_relieving_letter_org']) ? $value['experience_relieving_letter_org'] : ''; ?>" class="form-control" readonly />
                                                                <input type="file" name="experience_relieving_letter[]" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group upload-files-custom">
                                                                <label class="control-label">Experience Certificate</label>
                                                                <input type="text" name="experience_certificate_org[]" value="<?php echo isset($value['experience_certificate_org']) ? $value['experience_certificate_org'] : ''; ?>" class="form-control" readonly />
                                                                <input type="file" name="experience_certificate[]" class="form-control" />
                                                            </div>
                                                        </div>                                                        
                                                    </div>  
                                                    </div>                                              
                                                <?php } ?>
                                            
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="button" value="Add Experience" id="addExperienceButton" class="btn btn-sm btn-secondary" />
                                                    <!-- <input type="button" value="Remove Experience" id="removeExperienceButton" class="btn btn-sm btn-danger" /> -->
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                    <?php } ?>

                                    <h3>References (Please provide 2 professional / 2 academic references (incase this is your first job))</h3>

                                    <div class="row">
                                        <div class="col-md-12 references-documents">

                                            <?php foreach ($step6_reference as $key => $value) { ?>
                                            <div class="box-div-wrapper">
                                                <div class="row">
                                                    <div class="col-md-12"><label class="control-label">Reference #<?php echo $key + 1; ?>: </label></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Reference Name <em>*</em></label>
                                                            <input type="text" name="reference_name[]" value="<?php echo isset($value['reference_name']) ? $value['reference_name'] : ''; ?>" required="required" class="form-control" placeholder="Reference Name #<?php echo $key + 1 ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Organization / Institution <em>*</em></label>
                                                            <input type="text" name="reference_organization_institution[]" value="<?php echo isset($value['reference_organization_institution']) ? $value['reference_organization_institution'] : ''; ?>" required="required" class="form-control" placeholder="Organization / Institution" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Designation <em>*</em></label>
                                                            <input type="text" name="reference_designation[]" value="<?php echo isset($value['reference_designation']) ? $value['reference_designation'] : ''; ?>" required="required" class="form-control" placeholder="Designation" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone Number <em>*</em></label>
                                                            <input type="tel" maxlength="15" id="reference-phone-<?php echo $key; ?>" name="reference_phone[]" value="<?php echo isset($value['reference_phone']) ? $value['reference_phone'] : ''; ?>" required="required" pattern="[0-9+ ]{10,15}" class="form-control" placeholder="Phone Number" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Email ID <em>*</em></label>
                                                            <input type="email" name="reference_email[]" value="<?php echo isset($value['reference_email']) ? $value['reference_email'] : ''; ?>" required="required" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$" class="email-form-control" placeholder="Email ID" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>                                            
                                        </div>
                                    </div>

                                    <button class="btn btn-primary prevBtn btn-sm pull-left" type="button" >Prev</button>
                                    <input type="submit" name="submit" class="btn btn-success btn-sm pull-right" value="Save Changes" />
                                </form>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>
        </div>
        <!-- /.box-body -->
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="confirm-field-remove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-field-removeLabel">
    <div class="modal-content-wrapper">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <div class="modal-body">Are you sure you want to delete?</div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn pull-left">Cancel</button>
            <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete-field-record">Delete</button>            
        </div>
    </div>    
</div>

<div id="popup-alert-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="popup-alert-boxLabel">
    <div class="modal-content-wrapper">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <div class="modal-body"> </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
        </div>
    </div>    
</div>