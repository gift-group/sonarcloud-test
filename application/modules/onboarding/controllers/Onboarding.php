<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Onboarding extends CI_Controller {

    function __construct() {
        parent::__construct();
        //Checking user is login or not 
        is_login();
        
        $this->load->model("Onboarding_model");
    }

    public function index() {
        
        $user_type = $this->session->get_userdata()['user_details'][0]->user_type;
        $allowed_user_type_array = array('admin', 'Member', 'Manager');
        
        if (in_array($user_type, $allowed_user_type_array)) {
            $result = array();
            $step2 = array();
            $this->load->view("include/header");
            $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
            $data["result"] = $this->Onboarding_model->get_onboarding_step1_data($users_id)[0];
            $data["step2"] = $this->Onboarding_model->get_onboarding_step2_data($users_id);
            $data["step3"] = $this->Onboarding_model->get_onboarding_step3_data($users_id);
            $data["step4"] = $this->Onboarding_model->get_onboarding_step4_data($users_id);
            $data["step5"] = $this->Onboarding_model->get_onboarding_step5_data($users_id);
            $data["step6_experience"] = $this->Onboarding_model->get_onboarding_step6_data($users_id);
            $data["step6_reference"] = $this->Onboarding_model->get_onboarding_step6_ref_data($users_id);

            $step2 = array();
            foreach ($data["step2"] as $key => $value) {
                $step2[$value['document_type']] = $value['document_name_org'];
            }
            $data['step2'] = $step2;

            if (empty($data["step3"])) {
                $data['step3'] = array(0 => array("member_fname" => ""));
            }

            if (empty($data["step4"])) {
                $data['step4'] = array(0 => array("edu_qualification" => ""));
            }

            if (empty($data["step5"])) {
                $data['step5'] = array(0 => array("pro_qualification" => ""));
            }

            if (empty($data["step6_experience"])) {
                $data['step6_experience'] = array(0 => array("experience_organization" => ""));
            }

            if (empty($data["step6_reference"])) {
                $data['step6_reference'] = array(0 => array("reference_name" => ""), 1 => array("reference_name" => ""));
            }

            $this->load->view("index", $data);
            $this->load->view("include/footer");
        } else {
            $this->session->set_flashdata('messagePr', 'You don\'t have permission to access.');
            redirect(base_url() . 'user/profile', 'refresh');
        }
    }

    public function save_onboarding_data_step1() {

        $data = array();
        $userdata = array();
        $data = $this->input->post();
        
//        $inserted_date_of_birth = DateTime::createFromFormat('d/m/Y', $data['date_of_birth']);
//        $data['date_of_birth'] = $inserted_date_of_birth->format('Y-m-d');

        unset($data['submit']);

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $update = $this->Onboarding_model->insert_or_update_onboarding_step1($users_id, $data);

        if ($update) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $this->update_steps(2);
        } else {
            $this->session->set_flashdata('message', 'Error');
        }

        redirect(base_url() . 'onboarding', 'refresh');
    }

    public function save_onboarding_data_step2() {

        $data = array();
        $data = $this->input->post();
        unset($data['submit']);

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;

        $general_docs_array = array("salary_slip1" => 1, "salary_slip2" => 2, "salary_slip3" => 3, "aadhaar_card_front" => 4, "aadhaar_card_back" => 5, "pan_card" => 6, "dob_certificate" => 7, "bank_statement" => 8, "form_16" => 9, 'latest_salary_revision_letter' => 10);
        $general_docs = array();

        if (!empty($_FILES)) {

            $index = 0;
            foreach ($_FILES as $key => $value) {
                if (!empty($value['name'])) {
                    $fileName = $this->uploadDocuments_step2($key, $key);

                    $general_docs[$index]['document_type'] = $key;
                    $general_docs[$index]['field_id'] = $general_docs_array[$key];
                    $general_docs[$index]['document_name_new'] = $fileName;
                    $general_docs[$index]['document_name_org'] = $value['name'];
                    $general_docs[$index]['users_id'] = $users_id;
                    $general_docs[$index]['created_by'] = $users_id;
                    $general_docs[$index]['updated_by'] = $users_id;

                    $index++;
                }
            }
        }

        $update = $this->Onboarding_model->insert_or_update_onboarding_step2($users_id, $general_docs);

        if ($update) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $this->update_steps(3);
        } else {
            $this->session->set_flashdata('message', 'Error');
        }

        redirect(base_url() . 'onboarding', 'refresh');
    }

    public function save_onboarding_data_step3() {

        $data = array();
        $data = $this->input->post();
        unset($data['submit']);

        $family_data = array();
        foreach ($data as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $family_data[$key1][$key] = $value1;

                if (!empty($_FILES['member_aadhaar_card_front']['name'][$key1])) {

                    $member_name = $data['member_fname'][$key1] . ' ' . $data['member_lname'][$key1] . ' Family Member Aadhaar Card Front';

                    $fileName = $this->uploadDocuments('member_aadhaar_card_front', $member_name, $key1);
                    $family_data[$key1]['member_aadhaar_card_front'] = $fileName;
                    $family_data[$key1]['member_aadhaar_card_front_org'] = $_FILES['member_aadhaar_card_front']['name'][$key1];
                }

                if (!empty($_FILES['member_aadhaar_card_back']['name'][$key1])) {

                    $member_name = $data['member_fname'][$key1] . ' ' . $data['member_lname'][$key1] . ' Family Member Aadhaar Card Back';

                    $fileName = $this->uploadDocuments('member_aadhaar_card_back', $member_name, $key1);
                    $family_data[$key1]['member_aadhaar_card_back'] = $fileName;
                    $family_data[$key1]['member_aadhaar_card_back_org'] = $_FILES['member_aadhaar_card_back']['name'][$key1];
                }
            }
        }

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $update = $this->Onboarding_model->insert_or_update_onboarding_step3($users_id, $family_data);

        if ($update) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $this->update_steps(4);
        } else {
            $this->session->set_flashdata('message', 'Error');
        }

        redirect(base_url() . 'onboarding', 'refresh');
    }

    public function save_onboarding_data_step4() {

        $data = array();
        $data = $this->input->post();
        unset($data['submit']);

        $edu_qualification_data = array();

        foreach ($data as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $edu_qualification_data[$key1][$key] = $value1;

                if (!empty($_FILES['edu_qualification_docs']['name'][$key1])) {

                    $qualification_name = $data['edu_qualification'][$key1] . ' Certificate';

                    if (strtolower($qualification_name) == 'other') {
                        $qualification_name = $data['edu_qualification_other'][$key1] . ' Educational Certificate';
                        ;
                    }

                    $fileName = $this->uploadDocuments('edu_qualification_docs', $qualification_name, $key1);
                    $edu_qualification_data[$key1]['edu_qualification_docs'] = $fileName;
                    $edu_qualification_data[$key1]['edu_qualification_docs_org'] = $_FILES['edu_qualification_docs']['name'][$key1];
                }
            }
        }

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $update = $this->Onboarding_model->insert_or_update_onboarding_step4($users_id, $edu_qualification_data);

        if ($update) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $this->update_steps(5);
        } else {
            $this->session->set_flashdata('message', 'Error');
        }

        redirect(base_url() . 'onboarding', 'refresh');
    }

    public function save_onboarding_data_step5() {

        $data = array();
        $data = $this->input->post();
        unset($data['submit']);

        $pro_qualification_data = array();

        foreach ($data as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $pro_qualification_data[$key1][$key] = $value1;

                if (!empty($_FILES['pro_qualification_docs']['name'][$key1])) {

                    $qualification_name = $data['pro_qualification'][$key1] . ' Professional Certificate';

                    $fileName = $this->uploadDocuments('pro_qualification_docs', $qualification_name, $key1);
                    $pro_qualification_data[$key1]['pro_qualification_docs'] = $fileName;
                    $pro_qualification_data[$key1]['pro_qualification_docs_org'] = $_FILES['pro_qualification_docs']['name'][$key1];
                }
            }
        }

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $update = $this->Onboarding_model->insert_or_update_onboarding_step5($users_id, $pro_qualification_data);

        if ($update) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $this->update_steps(6);
        } else {
            $this->session->set_flashdata('message', 'Error');
        }

        redirect(base_url() . 'onboarding', 'refresh');
    }

    public function save_onboarding_data_step6() {

        $data = array();
        $data = $this->input->post();
        unset($data['submit']);

        $reference_array = array();
        $reference_array['reference_name'] = $data['reference_name'];
        $reference_array['reference_organization_institution'] = $data['reference_organization_institution'];
        $reference_array['reference_designation'] = $data['reference_designation'];
        $reference_array['reference_phone'] = $data['reference_phone'];
        $reference_array['reference_email'] = $data['reference_email'];

        unset($data['reference_name']);
        unset($data['reference_organization_institution']);
        unset($data['reference_designation']);
        unset($data['reference_phone']);
        unset($data['reference_email']);

        $experience_data = array();
        foreach ($data as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $experience_data[$key1][$key] = $value1;

                if (!empty($_FILES['experience_relieving_letter']['name'][$key1])) {

                    $org_name = $data['experience_organization'][$key1] . ' Relieving Letter';

                    $fileName = $this->uploadDocuments('experience_relieving_letter', $org_name, $key1);
                    $experience_data[$key1]['experience_relieving_letter_new'] = $fileName;
                    $experience_data[$key1]['experience_relieving_letter_org'] = $_FILES['experience_relieving_letter']['name'][$key1];
                }

                if (!empty($_FILES['experience_certificate']['name'][$key1])) {

                    $org_name = $data['experience_organization'][$key1] . ' Experience Certificate';

                    $fileName = $this->uploadDocuments('experience_certificate', $org_name, $key1);
                    $experience_data[$key1]['experience_certificate_new'] = $fileName;
                    $experience_data[$key1]['experience_certificate_org'] = $_FILES['experience_certificate']['name'][$key1];
                }
            }
        }

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $update = $this->Onboarding_model->insert_or_update_onboarding_step6($users_id, $experience_data);

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $reference_data = array();

        foreach ($reference_array as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $reference_data[$key1][$key] = $value1;
            }
        }

        $update1 = $this->Onboarding_model->insert_or_update_onboarding_step6_ref($users_id, $reference_data);

        if ($update && $update1) {
            $this->session->set_flashdata('message', 'Your onboarding application successfully submitted.');
            $this->update_steps(6);

            redirect(base_url() . 'thankyou', 'refresh');
        } else {
            $this->session->set_flashdata('message', 'Error');
        }

        redirect(base_url() . 'onboarding', 'refresh');
    }

    /**
     * This function is used to Upload file
     * @param $fielName : This is input name from form
     * @return fileName by which file is uploaded on server
     */
    public function uploadDocuments($fielName, $newFileName, $index) {

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;

        $filename = $_FILES[$fielName]['name'][$index];
        $tmpname = $_FILES[$fielName]['tmp_name'][$index];

        $exp = explode('.', $filename);
        $ext = end($exp);
        //$newname = $exp[0] . '_' . time() . "." . $ext;

        $newFileName = str_replace('', ' ', $newFileName);
        $newname = ucwords(strtolower($newFileName)) . '' . $index . "." . $ext;

        if (!is_dir("assets/documents/" . $users_id . "/")) {
            mkdir("assets/documents/" . $users_id . "/", 0777, TRUE);
        }

        $config['upload_path'] = "assets/documents/" . $users_id . "/";
        $config['upload_url'] = base_url() . "assets/documents/" . $users_id . "/";
        $config['allowed_types'] = "gif|jpg|jpeg|png|ico";
        $config['max_size'] = '2000000';
        $config['file_name'] = $newname;
        $this->load->library('upload', $config);
        $file_uploaded = move_uploaded_file($tmpname, "assets/documents/" . $users_id . "/" . $newname);

        if ($file_uploaded) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $filenewname = $newname;
        } else {
            $this->session->set_flashdata('message', 'Error');
            $filenewname = '';
        }
        
        return $filenewname;
    }

    /**
     * This function is used to Upload file
     * @param $fielName : This is input name from form
     * @return fileName by which file is uploaded on server
     */
    public function uploadDocuments_step2($fielName, $newFileName) {

        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;

        $filename = $_FILES[$fielName]['name'];
        $tmpname = $_FILES[$fielName]['tmp_name'];

        $exp = explode('.', $filename);
        $ext = end($exp);
        //$newname = $exp[0] . '_' . time() . "." . $ext;

        $newFileName = str_replace('_', ' ', $newFileName);
        $newname = ucwords(strtolower($newFileName)) . "." . $ext;

        if (!is_dir("assets/documents/" . $users_id . "/")) {
            mkdir("assets/documents/" . $users_id . "/", 0777, TRUE);
        }

        $file_exists = FCPATH . "assets/documents/" . $users_id . "/" . ucfirst(strtolower($newFileName)) . '.*';
        $files = glob($file_exists);

        if (isset($files)) {
            foreach ($files as $key => $value) {
                if (file_exists($value)) {
                    unlink($value);
                } else {
                    // File not found.
                }
            }
        }

        $config['upload_path'] = "assets/documents/" . $users_id . "/";
        $config['upload_url'] = base_url() . "assets/documents/" . $users_id . "/";


        $config['allowed_types'] = "gif|jpg|jpeg|png|ico";
        $config['max_size'] = '2000000';
        $config['file_name'] = $newname;
        $this->load->library('upload', $config);
        $file_uploaded = move_uploaded_file($tmpname, "assets/documents/" . $users_id . "/" . $newname);

        if ($file_uploaded) {
            $this->session->set_flashdata('message', 'Your data updated Successfully..');
            $filenewname = $newname;
        } else {
            $this->session->set_flashdata('message', 'Error');
            $filenewname = '';
        }
        return $filenewname;
    }

    private function update_steps($step_number) {

        $user_steps = array();
        $user_steps['current_step'] = $step_number;
        $users_id = $this->session->get_userdata()['user_details'][0]->users_id;
        $current_step = $this->session->get_userdata()['current_step'];
        if ($step_number > $current_step) {
            $res = $this->Onboarding_model->update_current_step_of_user($users_id, $user_steps);
            if ($res) {
                $cur_user_details = $this->session->get_userdata()['current_step'];
                $this->session->unset_userdata('current_step');
                $this->session->set_userdata('current_step', $step_number);
            }
        }
    }

}