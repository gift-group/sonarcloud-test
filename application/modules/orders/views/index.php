<!-- page content -->
<!-- Content Wrapper. Contains page content -->
<div id="content-outer-div" class="content-wrapper onboarding-page">
    <!-- Main content -->
    <section class="content">
        <?php
        if ($this->session->flashdata("message")) { ?>
            <div class="alert <?php echo ($this->session->flashdata("message") == "Error") ? "alert-danger" : "alert-success" ?> alert-dismissible" role="alert">      
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata("message") ?>
            </div>
        <?php } ?>        
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="confirm-field-remove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-field-removeLabel">
    <div class="modal-content-wrapper">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <div class="modal-body">Are you sure you want to delete?</div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn pull-left">Cancel</button>
            <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete-field-record">Delete</button>            
        </div>
    </div>    
</div>

<div id="popup-alert-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="popup-alert-boxLabel">
    <div class="modal-content-wrapper">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <div class="modal-body"> </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
        </div>
    </div>    
</div>