<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Orders extends CI_Controller {

    function __construct() {
        parent::__construct();
        //Checking user is login or not 
        is_login();
        
        $this->load->model("Orders_model");
    }

    public function index() {
        
        $user_type = $this->session->get_userdata()['user_details'][0]->user_type;
        $allowed_user_type_array = array('admin', 'Member', 'Manager');
        
        if (in_array($user_type, $allowed_user_type_array)) {

            $this->load->view("include/header");
            $data = "";

            $this->load->view("index", $data);
            $this->load->view("include/footer");
        } else {
            $this->session->set_flashdata('messagePr', 'You don\'t have permission to access.');
            redirect(base_url() . 'user/profile', 'refresh');
        }
    }
}