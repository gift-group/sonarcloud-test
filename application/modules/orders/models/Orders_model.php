<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Documents_model Class extends CI_Model
 */
class Orders_model extends CI_Model {
    
    function __construct() {
        
        parent::__construct();
        $this->load->database();
    }
}