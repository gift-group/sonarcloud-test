<!-- page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper settingPage">
<!-- Main content -->
<section class="content thankyou-page">
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">              
                    <div id="thank-you-onboarding" class="second-section-inner">
                        <h1>Thank you!</h1>
                        <h3>Your documents has been submitted successfully.</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->