<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Onboarding Form Create PDF</title>

        <style type="text/css">
            body {
                background-color: #fff;
                font: 12px/20px normal Arial;
                color: #000000;
            }
            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }
            #container header{
                float: left;
                width: 100%;
            }
            #container header .logo{
                float: left; 
                width: 75%;
            }
            #container header .employee-photo{
                float: left;
                width: 25%;
                text-align: right;
            }
            h1{
                font-size: 18px;
                text-transform: uppercase;
                text-align: center;
                width: 100%;
            }
            #onboarding-body {
                float: left;
                width: 100%;
                margin-top: 50px;
            }
            .pdf-form-fields{
                float: left;
                width: 100%;
                margin-bottom: 15px;
                font-weight: bold;
            }
            .pdf-form-fields span{
                font-weight: normal;
                /*border-bottom: 1px solid #333333;*/
                letter-spacing: 1.2px;
                text-transform: capitalize;
            }
            .pdf-form-fields span.lowercase-letters, .pdf-form-fields td.lowercase-letters{
                text-transform: lowercase;
            }
            .pdf-form-fields span.uppercase-letters{
                text-transform: uppercase;
            }
            .pdf-form-fields table td{
                text-transform: capitalize;
            }
            .pdf-form-fields .one-half{
                float: left;
                width: 50%;
            }
            footer .pdf-form-fields span{
                border-bottom: 0 none;
            }
            footer .pdf-form-fields .text-align-center{
                text-align: center;
            }            
        </style>
    </head>
    <body>
        <div id="container">
            <header>
                <div class="logo">
                    <img src="assets/images/black-logo.jpg" class="img-responsive" alt="Gift Group Logo" />
                    <hr>
                    <h1>Joining report & employee/ contractual information sheet</h1>
                </div>
                <div class="employee-photo">
                    <?php
                    if (file_exists('assets/images/' . $db_data['userDetails']['profile_pic']) && isset($db_data['userDetails']['profile_pic'])) {
                        $profile_pic = $db_data['userDetails']['profile_pic'];
                    } else {
                        $profile_pic = 'user.png';
                    }
                    ?>
                    <img style="max-width: 120px; max-height: 148px;" src="<?php echo base_url(); ?>/assets/images/<?php echo isset($profile_pic) ? $profile_pic : 'user.png'; ?>" class="img-responsive" alt="Employee Identity Photo" />
                </div>                
            </header>

            <section id="onboarding-body">
                <div class="pdf-form-fields">
                    DATE OF JOINING: <span><?php echo $db_data['userDetails']['date_of_joining']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    FULL NAME: <span><?php echo $db_data['userDetails']['employee_name']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    FATHER’S/ HUSBAND'S NAME: <span><?php echo $db_data['userDetails']['father_name']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    <?php
                    $dateOfBirthEmp = $db_data['userDetails']['date_of_birth'];
                    $todayEmp = date("d-M-Y");
                    $diffAgeEmp = date_diff(date_create($dateOfBirthEmp), date_create($todayEmp));
                    ?>
                    DATE OF BIRTH: <span><?php echo $db_data['userDetails']['date_of_birth']; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age: <span><?php echo $diffAgeEmp->format('%y'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GENDER: <span><?php echo $db_data['userDetails']['gender']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    CONTACT ADDRESS: <span><?php echo $db_data['userDetails']['contact_address'] .', '. $db_data['userDetails']['contact_city'] .', '. $db_data['userDetails']['contact_state'] .' - '. $db_data['userDetails']['contact_pin_code'].'.'; ?></span>
                </div>
                <div class="pdf-form-fields">
                    PERMANENT ADDRESS: <span><?php echo $db_data['userDetails']['permanent_address'] .', '. $db_data['userDetails']['permanent_city'] .', '. $db_data['userDetails']['permanent_state'] .' - '. $db_data['userDetails']['permanent_pin_code'].'.'; ?></span>
                </div>
                <div class="pdf-form-fields">
                    <div class="one-half">CONTACT #1: <span><?php echo $db_data['userDetails']['contact_no1']; ?></span></div>
                    <div class="one-half">CONTACT #2: <span><?php echo !empty($db_data['userDetails']['contact_no2']) ? $db_data['userDetails']['contact_no2'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>
                </div>
                <div class="pdf-form-fields">
                    ICE (In Case of Emergency) NAME: <span><?php echo $db_data['userDetails']['ice_name']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    ICE CONTACT NO.: <span><?php echo $db_data['userDetails']['ice_contact_no']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    ICE Contact Relationship: <span><?php echo $db_data['userDetails']['ice_relationship']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    E-MAIL ID: <span class="lowercase-letters"><?php echo $db_data['userDetails']['email']; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;BLOOD GROUP: <span class="uppercase-letters"><?php echo $db_data['userDetails']['blood_group']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    MARITAL STATUS: <span><?php echo $db_data['userDetails']['marital_status']; ?></span>
                </div>
                <div class="pdf-form-fields">                    
                    SPOUSE NAME: <span><?php if(!empty($db_data['userDetails']['spouse_name'])){ echo $db_data['userDetails']['spouse_name']; } else { echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; } ?></span>
                </div>
                <div class="pdf-form-fields">   
                    <?php
                    if (!empty($db_data['childrens'])) {
                        $children_count = count($db_data['childrens']);
                    } else {
                        $children_count = 0;
                    } ?>
                    NO. OF CHILDREN: <font weight="normal"><?php echo $children_count; ?></font>
                </div>

                <!--
                <?php
                if (!empty($db_data['childrens'])) {
                    foreach ($db_data['childrens'] as $key => $value) {
                        ?>
                        <div class="pdf-form-fields">
                            NAME OF CHILDREN #<?php echo $key + 1; ?>: <span><?php echo $value['member_name']; ?></span>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <div class="pdf-form-fields">
                        NAME OF CHILDREN: <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </div>
                <?php } ?>
                -->
                <div class="pdf-form-fields">
                    NATIONALITY: <span><?php echo $db_data['userDetails']['nationality']; ?></span>
                </div>
                <div class="pdf-form-fields">
                    <div class="one-half">PAN NO.: <span class="uppercase-letters"><?php echo !empty($db_data['userDetails']['pan_number']) ? $db_data['userDetails']['pan_number'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>
                    <div class="one-half">AADHAAR NO.: <span class="uppercase-letters"><?php echo !empty($db_data['userDetails']['aadhaar_number']) ? $db_data['userDetails']['aadhaar_number'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>                    
                </div>
                <div class="pdf-form-fields">
                    <div class="one-half">PF NO.: <span class="uppercase-letters"><?php echo !empty($db_data['userDetails']['pf_number']) ? $db_data['userDetails']['pf_number'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>
                    <div class="one-half">UAN NO.: <span class="uppercase-letters"><?php echo !empty($db_data['userDetails']['uan_number']) ? $db_data['userDetails']['uan_number'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>                    
                </div>
                <div class="pdf-form-fields">
                    PASSPORT NO.: <span class="uppercase-letters"><?php echo !empty($db_data['userDetails']['passport_number']) ? $db_data['userDetails']['passport_number'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span>
                </div>
                <div class="pdf-form-fields">
                    <div class="one-half">PASSPORT ISSUE DATE: <span><?php echo !empty($db_data['userDetails']['passport_issue_date']) ? $db_data['userDetails']['passport_issue_date'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>
                    <div class="one-half">PASSPORT EXPIRY DATE: <span><?php echo !empty($db_data['userDetails']['passport_expiry_date']) ? $db_data['userDetails']['passport_expiry_date'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>
                </div>
                <div class="pdf-form-fields">
                    HOBBIES: <span><?php echo !empty($db_data['userDetails']['hobbies']) ? $db_data['userDetails']['hobbies'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span>
                </div>

                <pagebreak>

                    <div class="pdf-form-fields">
                        <h3>FAMILY DETAILS FOR MEDICAL INSURANCE:</h3>
                    </div>
                    <div class="pdf-form-fields">
                        <table width="100%" border="1" cellspacing="0" cellpadding="5">
                            <tbody>
                                <tr>
                                    <th align="center">NAME</th>
                                    <th align="center">DOB</th>
                                    <th align="center">RELATION</th>
                                    <th align="center">OCCUPATION</th>
                                </tr>

                                <?php
                                if (!empty($db_data['family'])) {
                                    foreach ($db_data['family'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['member_name']; ?></td>
                                            <td><?php echo $value['member_date_of_birth']; ?></td>
                                            <td><?php echo $value['member_relationship']; ?></td>
                                            <td><?php echo $value['member_occupation']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td>--</td>
                                            <td>--</td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="pdf-form-fields">
                        <h3>EDUCATION QUALIFICATION (Start with the Latest Qualification attained):</h3>
                    </div>
                    <div class="pdf-form-fields">
                        <table width="100%" border="1" cellspacing="0" cellpadding="5">
                            <tbody>
                                <tr>
                                    <th align="center">QUALIFICATION</th>
                                    <th align="center">UNIVERSITY / INSTITUTE</th>
                                    <th align="center">YEAR OF PASSING</th>
                                    <th align="center">% MARKS</th>
                                    <th align="center">SPECIALIZATION</th>
                                </tr>
                                <?php
                                if (!empty($db_data['eduQualification'])) {
                                    foreach ($db_data['eduQualification'] as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $value['edu_qualification']; ?></td>
                                        <td><?php echo $value['edu_qualification_university']; ?></td>
                                        <td align="center"><?php echo $value['edu_qualification_passing_year']; ?></td>
                                        <td align="center"><?php echo $value['edu_qualification_percentage_marks']; ?></td>
                                        <td><?php echo $value['edu_qualification_specialization']; ?></td>
                                    </tr>
                                <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td>--</td>
                                            <td>--</td>
                                            <td align="center">--</td>
                                            <td align="center">--</td>
                                            <td>--</td>
                                        </tr>
                                <?php } ?>                                
                            </tbody>
                        </table>
                    </div>

                    <div class="pdf-form-fields">
                        <h3>PROFESSIONAL QUALIFICATION:</h3>
                    </div>
                    <div class="pdf-form-fields">
                        <table width="100%" border="1" cellspacing="0" cellpadding="5">
                            <tbody>
                                <tr>
                                    <th align="center">QUALIFICATION</th>
                                    <th align="center">UNIVERSITY / INSTITUTE</th>
                                    <th align="center">YEAR OF PASSING</th>
                                    <th align="center">% MARKS</th>
                                    <th align="center">SPECIALIZATION</th>
                                </tr>
                                <?php
                                if (!empty($db_data['proQualification'])) {
                                    foreach ($db_data['proQualification'] as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $value['pro_qualification']; ?></td>
                                        <td><?php echo $value['pro_qualification_university']; ?></td>
                                        <td align="center"><?php echo $value['pro_qualification_passing_year']; ?></td>
                                        <td align="center"><?php echo $value['pro_qualification_percentage_marks']; ?></td>
                                        <td><?php echo $value['pro_qualification_specialization']; ?></td>
                                    </tr>
                                <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td>--</td>
                                            <td>--</td>
                                            <td align="center">--</td>
                                            <td align="center">--</td>
                                            <td>--</td>
                                        </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="pdf-form-fields">
                        <h3>EXPERIENCE (CHRONOLOGICAL ORDER):</h3>
                    </div>
                    <div class="pdf-form-fields">
                        <table width="100%" border="1" cellspacing="0" cellpadding="5">
                            <tbody>
                                <tr>
                                    <th align="center">ORGANISATION</th>
                                    <th colspan="2" align="center">PERIOD</th>
                                    <th align="center">DESIGNATION</th>
                                    <!-- <th align="center">JOB RESPONSIBILITY</th> -->
                                    <th align="center">GROSS SALARY/MONTH</th>
                                    <th align="center">REASON FOR LEAVING</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th align="center">FROM</th>
                                    <th align="center">TO</th>
                                    <th></th>
                                    <!-- <th></th> -->
                                    <th></th>
                                    <th></th>
                                </tr>
                                <?php
                                $MonthArray = array(
                                                    "1" => "Jan", "2" => "Feb", "3" => "Mar", "4" => "Apr",
                                                    "5" => "May", "6" => "Jun", "7" => "Jul", "8" => "Aug",
                                                    "9" => "Sept", "10" => "Oct", "11" => "Nov", "12" => "Dec",
                                                );
                                
                                $total_expr = 0;
                                if (!empty($db_data['experience'])) {
                                    foreach ($db_data['experience'] as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $value['experience_organization']; ?></td>
                                        <td align="center"><?php echo $MonthArray[$value['experience_period_from_month']] .' '. $value['experience_period_from_year']; ?></td>
                                        <td align="center"><?php echo $MonthArray[$value['experience_period_to_month']] .' '. $value['experience_period_to_year']; ?></td>
                                        <td><?php echo $value['experience_designation']; ?></td>
                                        <!-- <td><?php //echo $value['experience_job_responsibility']; ?></td> -->
                                        <td align="center"><?php echo $value['experience_gross_salary_month']; ?></td>
                                        <td><?php echo $value['experience_reason_for_leaving']; ?></td>
                                    </tr>
                                <?php
                                
                                $total_expr = $value['total_experience'];                                
                                    } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td>--</td>
                                            <td align="center">--</td>
                                            <td align="center">--</td>
                                            <td>--</td>
                                            <td>--</td>
                                            <td align="center">--</td>
                                            <td>--</td>
                                        </tr>
                                <?php } ?>                                
                                <tr>
                                    <?php $total_experience = floor($total_expr / 12) .' Years '. $total_expr % 12 .' Months'; ?>
                                    <td colspan="7"><strong>Total Yrs of Experience:</strong> <?php echo $total_experience; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <pagebreak>
                        
                    <div class="pdf-form-fields">
                        <h3>REFERENCES (Please provide 2 professional / 2 academic references (incase this is your first job):</h3>
                    </div>
                    <div class="pdf-form-fields">
                        <table width="100%" border="1" cellspacing="0" cellpadding="5">
                            <tbody>
                                <tr>
                                    <th align="center">NAME</th>
                                    <th align="center">ORGANIZATION/ INSTITUTION</th>
                                    <th align="center">DESIGNATION</th>
                                    <th align="center">PHONE</th>
                                    <th align="center">EMAIL</th>
                                </tr>
                                <?php
                                if (!empty($db_data['reference'])) {
                                    foreach ($db_data['reference'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['reference_name']; ?></td>
                                            <td><?php echo $value['reference_organization_institution']; ?></td>
                                            <td><?php echo $value['reference_designation']; ?></td>
                                            <td><?php echo $value['reference_phone']; ?></td>
                                            <td class="lowercase-letters"><?php echo $value['reference_email']; ?></td>
                                        </tr>
                                <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td>--</td>
                                            <td>--</td>
                                            <td>--</td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
            </section>

            <footer>
                <div class="pdf-form-fields">
                    <h3>DECLARATION:</h3>

                    <p>I DECLARE THAT THE ABOVE INFORMATION GIVEN BY ME IS TRUE AND THE INFORMATION THAT I HAVE GIVEN IN THIS APPLICATION IS NOT FALSE OR MISLEADING. I UNDERSTAND THAT ANIBRAIN HAS THE RIGHT TO VERIFY ANY INFORMATION PROVIDED HEREIN. IF ANY FALSE / MISLEADING INFORMATION IS FOUND ANIBRAIN RESERVES THE RIGHT TO TAKE AN APPROPRIATE ACTION INCLUDING TERMINATION OF EMPLOYMENT WITHOUT ADVANCE NOTICE.</p>
                </div>
                <div class="pdf-form-fields">&nbsp;</div>
                <div class="pdf-form-fields">
                    <div class="one-half">DATE: <span><?php echo $db_data['userDetails']['updated_on']; ?></span></div>
                    <div class="one-half text-align-center"><span><?php echo ($db_data['userDetails']['employee_name']) ? $db_data['userDetails']['employee_name'] : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; ?></span></div>
                    <div class="one-half">PLACE: <span>Pune</span></div>
                    <div class="one-half text-align-center">SIGNATURE OF APPLICANT</div>
                </div>
            </footer>            
        </div>
    </body>
</html>