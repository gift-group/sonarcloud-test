<footer class="main-footer">All rights reserved Ⓒ www.giftgp.com.</footer>
    <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/extras/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/extras/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/extras/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/extras/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/extras/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/extras/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/extras/buttons.print.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/daterangepicker.js'); ?>"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.form-validator.min.js');?>"></script>

<!-- SlimScroll -->
<script src="<?php echo base_url('assets/js/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/js/fastclick.js');?>"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/js/app.min.js');?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/js/icheck.min.js');?>"></script>
<script src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>"></script>
<script src="<?php echo base_url('assets/ckeditor/adapters/jquery.js');?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/js/demo.js');?>"></script>
<script src="<?php echo base_url('assets/js/custom.js');?>"></script>
<script src="<?php echo base_url('assets/js/onboarding.js');?>"></script>
<script>
    function validate_fileType(fileName,Nameid,arrayValu)
    {
        var string = arrayValu;
        var tempArray = new Array();
        var tempArray = string.split(',');                  
        var allowed_extensions =tempArray;
        var file_extension = fileName.split(".").pop(); 
        for(var i = 0; i <= allowed_extensions.length; i++)
        {
            if(allowed_extensions[i]==file_extension)
            {   
                $("#error_"+Nameid).html("");
                return true; // valid file extension
            }
        }
        $("#"+Nameid).val("");
        $("#error_"+Nameid).css("color","red").html("File format not support to upload");
        return false;
    }
</script>

<!-- modal -->
<div id="cnfrm_delete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-md-6">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-small yes-btn btn-danger" href="">Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div> 
    </div>
</div>

<!-- modal -->
<div id="cnfrm_verify" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-md-12">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Document Verification Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to verify given user documents.</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-small yes-btn btn-success" href="">Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
$.fn.dataTableExt.oApi.fnFilterAll = function(oSettings, sInput, iColumn, bRegex, bSmart) {
    var settings = $.fn.dataTableSettings;
    
    for (var i = 0; i < settings.length; i++) {
        settings[i].oInstance.fnFilter(sInput, iColumn, bRegex, bSmart);
    }
};

$(document).ready(function () {
    var url = '<?php echo base_url(); ?>';//$('.content-header').attr('rel');

    var table = $('#example1').dataTable({
        dom: 'lfBrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        searchDelay: 5000,
        "processing": true,
        "serverSide": true,
        "ajax": url + "user/dataTable",
        "sPaginationType": "full_numbers",
        "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Search",
            "infoFiltered": "",
            "paginate": {
                "next": '<i class="fa fa-angle-right"></i>',
                "previous": '<i class="fa fa-angle-left"></i>',
                "first": '<i class="fa fa-angle-double-left"></i>',
                "last": '<i class="fa fa-angle-double-right"></i>'
            }
        },
        "ordering": false,
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]]
    });

    var table1 = $('#invited-users').dataTable({
        dom: 'lfBrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        searchDelay: 5000,
        "processing": true,
        "serverSide": true,
        "ajax": url + "user/dataTableForInvitedUsers",
        "sPaginationType": "full_numbers",
        "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Search",
            "infoFiltered": "",
            "paginate": {
                "next": '<i class="fa fa-angle-right"></i>',
                "previous": '<i class="fa fa-angle-left"></i>',
                "first": '<i class="fa fa-angle-double-left"></i>',
                "last": '<i class="fa fa-angle-double-right"></i>'
            }
        },
        "ordering": false,
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]]
    });

    var table2 = $('#registered-users-list').dataTable({
        dom: 'lfBrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        searchDelay: 5000,
        "processing": true,
        "serverSide": true,
        "ajax": url + "user/dataTableForRegisteredUsers",
        "sPaginationType": "full_numbers",
        "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "Search",
            "infoFiltered": "",
            "paginate": {
                "next": '<i class="fa fa-angle-right"></i>',
                "previous": '<i class="fa fa-angle-left"></i>',
                "first": '<i class="fa fa-angle-double-left"></i>',
                "last": '<i class="fa fa-angle-double-right"></i>'
            }
        },
        "ordering": false,
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]]
    });

    // $('#invited-users thead th.search-box, #example1 thead th.search-box').each(function () {
    //     var title = $(this).text();
    //     $(this).html(title+' <input type="text" class="col-search-input" placeholder="Search ' + title + '" />');
    // });

    // table.columns().every(function () {
    //     var table = this;
    //     $('input', this.header()).on('keyup change', function () {
    //         if (table.search() !== this.value) {
    //             table.search(this.value).draw();
    //         }
    //     });
    // });
    
    // table1.columns().every(function () {
    //     var table1 = this;
    //     $('input', this.header()).on('keyup change', function () {
    //         if (table1.search() !== this.value) {
    //             table1.search(this.value).draw();
    //         }
    //     });
    // });

    $("#search-all").keyup(function(e) {
        if(this.value.length >= 3 || e.keyCode == 13) {
            table.fnFilterAll(this.value);
            table1.fnFilterAll(this.value);
            table2.fnFilterAll(this.value);    
        }

        // Ensure we clear the search if they backspace far enough
        if(this.value == "") {
            table.fnFilterAll('');
            table1.fnFilterAll('');
            table2.fnFilterAll('');
        }
    });

    setTimeout(function () {
        var add_width = $('.dataTables_filter').width() + $('.box-body .dt-buttons').width() + 10;
        $('.table-date-range').css('right', add_width + 'px');

        <?php
        $user_type = $this->session->get_userdata()['user_details'][0]->user_type;
        if($user_type == 'admin'){ ?>
            $('.dataTables_info').before('<button data-base-url="<?php echo base_url() . 'user/delete/'; ?>" rel="delSelTable" class="btn btn-default btn-sm delSelected pull-left btn-blk-del"> <i class="fa fa-trash"></i> </button><br><br>');
        <?php } ?>
    }, 300);
    $("button.closeTest, button.close").on("click", function () {});    
});

//Verify user documents
function verifyUserId(id) {
    var url =  "<?php echo site_url();?>";
    $("#cnfrm_verify").find("a.yes-btn").attr("href", url+"user/verifyUserDocuments?users_id="+id);
}
</script>
</body>
</html>